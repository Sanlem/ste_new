from abc import abstractmethod, ABC
from data.user import User
# A


class AbstractBaseNotificator(ABC):
    @abstractmethod
    async def notify(self, user: User, message: str):
        pass


class BaseNotificator(AbstractBaseNotificator):
    async def notify(self, user: User, message: str):
        pass

