import sys
import logging
import aiohttp
from .base import BaseNotificator
from data.user import User


log = logging.getLogger("notifications.telegram")


class TelegramNotificator(BaseNotificator):
    def __init__(self, url: str, bot_key: str):
        self.url = url
        self.bot_key = bot_key

    async def notify(self, user: User, message: str) -> bool:
        try:
            log.info(f"Gonna notify {user} with {message}.")
            url = self.url.format(self.bot_key, "sendMessage")
            msg = {
                "chat_id": user.telegram_id,
                "text": message,
            }
            async with aiohttp.ClientSession() as session:
                async with session.post(url, json=msg) as resp:
                    rj = await resp.json()
                    log.info(f"SendMessage {msg}: {rj}")
                    return True
        except Exception:
            log.error(f"Telegram notify {user} {message}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
        return False