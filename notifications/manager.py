import sys
import logging
from asyncio import get_event_loop, Queue
from data.notification_subscription import NotificationSubscription
from data.providers import OHLCVProvider
from data.user import User
from signals.signals import BaseSignal
from web.ws_handler import CH_CONF
from notifications.notificators import TelegramNotificator


log = logging.getLogger("notifications")


class NotificationsManager(object):
    def __init__(self, ohlcv_provider: OHLCVProvider, notification_conf: dict):
        self.conf = notification_conf
        self.should_stop = False
        self.in_queue = Queue()
        self.ohlcv_provider = ohlcv_provider
        self.loop = get_event_loop()
        self.last_message = {}
        """
        {
            <user_id>: {
                <signal_conf_key>: <last_message>
            }
        }
        """
        self.notifications_by_user = {}
        """
        {
            <user_id>: {
                <signal_conf_key>: <signal_provider>
            }
        }
        """

        self.notifications_by_key = {}
        """
        {
            <signal_conf_key>: [<notification_subscription>] 
            
            # {
            #     <user_id>: <queue>
            # }
        }
        """
        self.notificators = {
            "telegram": TelegramNotificator(self.conf["telegram"]["url"], self.conf["telegram"]["token"])
        }

    async def remove_notification(self, subscription: NotificationSubscription):
        try:
            subscr_type = subscription.sub_type
            conf_class = CH_CONF[subscr_type]["conf_class"]

            conf = conf_class.deserialize(**subscription.conf)
            key = conf.get_key()

            if key not in self.notifications_by_key:
                log.error(f"Try to remove unexisting notification {key}.")
                return

            self.notifications_by_key[key] = set([n for n in self.notifications_by_key[key] if n._id != subscription._id])
            print(self.notifications_by_key[key])
            await subscription.delete()
        except Exception:
            log.error(f"RemoveNotification {subscription}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def add_notification(self, subscription: NotificationSubscription):
        try:
            subscr_type = subscription.sub_type
            conf_class = CH_CONF[subscr_type]["conf_class"]

            conf = conf_class.deserialize(**subscription.conf)
            if conf is None:
                log.error(f"Failed to init conf for {subscription}")
                return

            provider_class = CH_CONF[subscr_type]["provider_class"]
            provider = provider_class(self.in_queue, conf, self.ohlcv_provider)

            user_id = subscription.user_id
            key = conf.get_key()
            if key not in self.notifications_by_key:
                self.notifications_by_key[key] = set()

            if user_id not in self.notifications_by_user:
                self.notifications_by_user[user_id] = {}

            if user_id not in self.notifications_by_key[key] and key not in self.notifications_by_user[user_id]:
                self.notifications_by_key[key].add(subscription)
                self.notifications_by_user[user_id][key] = provider
            else:
                log.warning(f"Try to duplicate subscription {subscription}!")
                return

            self.loop.create_task(provider.run())
            log.info(f"Loaded {subscription}!")
        except Exception:
            log.error(f"addSubscription {subscription}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    def should_notify(self, user: User, key: str, msg: str) -> bool:
        if user._id in self.last_message:
            return self.last_message[user._id].get(key, "-") != msg
        return True

    def update_last_sent(self, user: User, key: str, msg: str):
        if user._id not in self.last_message:
            self.last_message[user._id] = {}

        self.last_message[user._id][key] = msg

    async def process_signal(self, signal: BaseSignal):
        try:
            serialized = signal.serialize()
            key = serialized.pop("key")

            if serialized["signal"] == "-":
                log.info(f"Won't send useless signal {signal}")
                return

            msg = f"{key}: {serialized['signal']}, {serialized['comment']}"
            subscriptions = self.notifications_by_key[key]

            for subscription in subscriptions:
                notificator = self.notificators.get(subscription.send_to, None)
                if notificator is None:
                    log.error(f"Can't send notification to {subscription.send_to}!")
                    return

                user = await User.manager.load_one(_id=subscription.user_id)
                if user is None:
                    log.error(f"No user to send {subscription.serialize()}!")
                    return
                if self.should_notify(user, key, msg) is True:
                    notified = await notificator.notify(user, msg)
                    if notified is True:
                        self.update_last_sent(user, key, msg)
        except Exception:
            log.error(f"ProcessSignal {signal}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def run(self):
        try:
            # 1. Load and init all existing subscriptions
            existing_subscriptions = await NotificationSubscription.manager.load()
            log.info(f"Loaded {len(existing_subscriptions)} existing notifications subscriptions.")
            for subscription in existing_subscriptions:
                await self.add_notification(subscription)

            while self.should_stop is False:
                try:
                    signal = await self.in_queue.get()
                    self.in_queue.task_done()

                    await self.process_signal(signal)
                except Exception:
                    log.error(f"ProcessSig: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
        except Exception:
            log.error(f"Run: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
