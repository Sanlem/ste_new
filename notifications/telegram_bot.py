import sys
import logging
from asyncio import sleep
from typing import Optional
import aiohttp
from data.user import User
from notifications.notificators.telegram import TelegramNotificator


log = logging.getLogger(__name__)


class TelegramBot(object):
    def __init__(self, url: str, bot_key: str):
        self.url = url
        self.bot_key = bot_key
        self.offset = None
        self.timeout = 10
        self.stop = False
        self.notificator = TelegramNotificator(url, bot_key)

    async def get_updates(self) -> Optional[dict]:
        try:
            params = {
                "offset": self.offset,
                "timeout": self.timeout / 2.0,
            }

            updates = None
            url = self.url.format(self.bot_key, "getUpdates")
            async with aiohttp.ClientSession() as session:
                async with session.post(url, json=params) as resp:
                    updates = await resp.json()
                    # log.info(f"GetUpdates {params}: {updates}")

            return updates
        except Exception:
            log.error(f"GetUpdates: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_updates(self, updates: dict):
        try:
            if updates["ok"] is True:
                for update in updates["result"]:
                    u_id = int(update["update_id"])
                    self.offset = u_id + 1

                    if "message" in update:
                        text = update["message"]["text"]

                        user = await User.manager.load_one(_id=text)

                        if user is not None:
                            user_tg_id = update["message"]["from"]["id"]

                            user.telegram_id = user_tg_id
                            await user.save()
                            log.info(f"Saved TelegramID {user_tg_id} for user {user.login}")

                            await self.notificator.notify(user, f"Привіт, {user.login}!")
                        else:
                            log.info(f"Not user_id message: {update['message']['text']}")
        except Exception:
            log.error(f"ProcessUpdates {updates}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def run(self):
        log.info("Telegram bot started!")
        while self.stop is False:
            try:
                updates = await self.get_updates()
                if updates:
                    await self.process_updates(updates)

                await sleep(self.timeout)
            except Exception:
                log.error(f"Run: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

        log.info("Stopping Telegram bot...")
