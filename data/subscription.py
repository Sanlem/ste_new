from data.base import BaseModel


class Subscription(BaseModel):
    collection_name = "subscriptions"

    def __init__(self, user_id: str, sub_type: str, conf: dict, _id=None):
        super().__init__()
        self._id = _id
        self.user_id = user_id
        self.sub_type = sub_type
        self.conf = conf

    def serialize(self):
        return {
            "_id": str(self._id),
            "user_id": self.user_id,
            "sub_type": self.sub_type,
            "conf": self.conf,
        }

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "Subscription({} {} {} {})"\
            .format(self._id, self.user_id, self.sub_type, self.conf)


Subscription.init_manager()
