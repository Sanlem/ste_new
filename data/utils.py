import bson


def get_unique_id():
    return str(bson.ObjectId())
