from abc import ABC, abstractmethod
from typing import Type
import sys
import logging
from data.utils import get_unique_id
from db import db


class AbstractBaseModel(ABC):
    @abstractmethod
    def save(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    @classmethod
    @abstractmethod
    def load_by_id(cls, oid):
        pass

    @abstractmethod
    def serialize(self):
        pass

    @classmethod
    @abstractmethod
    def deserialize(cls, data):
        pass


class AbstractBaseManager(ABC):
    @abstractmethod
    def __init__(self, model_type, collection_name):
        self.model_type = model_type
        self.collection_name = collection_name
        self.db = db

        self.collection = db[collection_name]
        self.log = logging.getLogger("{}Manager".format(model_type.__name__))

    @classmethod
    @abstractmethod
    def get_manager(cls, model_type, collection_name):
        pass

    @abstractmethod
    def save(self, obj):
        pass

    @abstractmethod
    def _create(self, obj):
        pass

    @abstractmethod
    def _update(self, obj):
        pass

    @abstractmethod
    def delete(self, obj):
        pass

    @abstractmethod
    def load_by_id(self, oid):
        pass


class Manager(AbstractBaseManager):
    managers = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def get_manager(cls, model_type, collection_name, manager_class: Type["Manager"] = None):
        manager = Manager.managers.get(collection_name, None)

        if manager is None:

            if manager_class is None:
                manager = Manager(model_type, collection_name)
            else:
                manager = manager_class(model_type, collection_name)
            Manager.managers[collection_name] = manager

        return manager

    async def load_one(self, **kwargs):
        try:
            query = {"$query": kwargs, "$orderby": {"$natural": -1}}
            obj = await self.collection.find_one(query)

            if obj is not None:
                return self.model_type.deserialize(obj)
            else:
                return None
        except Exception:
            self.log.error(f"{self.model_type.__name__} LoadOne {kwargs}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def load(self, **kwargs):
        try:
            limit = kwargs.pop("limit", None)
            reverse = kwargs.get("reverse", False)

            query = {"$query": kwargs, "$orderby": {"$natural": -1}}
            cursor = self.collection.find(query)

            objects = await cursor.to_list(length=limit)

            deserialized = []
            for o in objects:
                deserialized.append(self.model_type.deserialize(o))

            if reverse is False:
                # reversed data here, just to select most recent
                deserialized.reverse()

            return deserialized
        except Exception as e:
            self.log.error("Load: {}".format(e))

    async def save(self, obj):
        if obj._id is None:
            oid = get_unique_id()
            obj._id = oid
            return await self._create(obj)
        else:
            return await self._update(obj)

    async def _create(self, obj):
        serialized = obj.serialize()
        return await self.collection.insert_one(serialized)

    async def _update(self, obj):
        serialized = obj.serialize()
        if "_id" not in serialized:
            raise AttributeError("Could not update not saved {}!".format(self.model_type))

        _id = serialized.pop("_id")
        return await self.collection.replace_one({"_id": _id}, serialized)

    async def delete(self, obj):
        serialized = obj.serialize()
        if "_id" not in serialized:
            raise AttributeError("Could not delete not saved {}!".format(self.model_type))

        _id = serialized.pop("_id")
        return await self.collection.delete_one({"_id": _id})

    async def load_by_id(self, _id):
        serialized = await self.collection.find_one({"_id": _id})
        return self.model_type.deserialize(serialized)


class BaseModel(AbstractBaseModel):
    manager = None
    collection_name = None

    @classmethod
    def init_manager(cls, manager_class: Type[Manager] = None):
        if cls.manager is None:
            cls.manager = Manager.get_manager(cls, cls.collection_name, manager_class)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # if type(self).manager is None:
        #     type(self).manager = Manager.get_manager(type(self), self.collection_name)

    async def load(self):
        return await self.manager.load()

    async def save(self):
        return await self.manager.save(self)

    async def delete(self):
        return await self.manager.delete(self)

    @classmethod
    async def load_by_id(cls, oid):
        return await cls.manager.load_by_id(oid)

    def serialize(self):
        raise NotImplementedError

    @classmethod
    def deserialize(cls, data):
        if "_id" in data:
            data["_id"] = str(data["_id"])
        else:
            print(f"Deserialize OHLCV with no id: {data}")

        return cls(**data)
