from datetime import datetime
from data.base import BaseModel


class OHLCV(BaseModel):
    collection_name = "ohlcv"

    def __init__(self, pair, market, timeframe, o, h, l, c, v, timestamp, _id=None):
        super().__init__()
        self._id = _id
        self.pair = pair
        self.market = market
        self.timeframe = timeframe
        self.o = float(o)
        self.h = float(h)
        self.l = float(l)
        self.c = float(c)
        self.v = float(v)
        self.timestamp = timestamp

    def serialize(self):
        return {
            "_id": str(self._id),
            "pair": self.pair,
            "market": self.market,
            "timeframe": self.timeframe,
            "o": self.o,
            "h": self.h,
            "l": self.l,
            "c": self.c,
            "v": self.v,
            "timestamp": self.timestamp
        }

    def humanized_timestamp(self):
        if self.timestamp is None:
            return "-"
        else:
            ts = datetime.utcfromtimestamp(self.timestamp)
            ts_hum = ts.strftime("%Y-%m-%d %H:%M:%S")
            return ts_hum

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "OHLCV({}, {}, {}, {}, {}, {}, {}, {}, {}, {})"\
            .format(self._id, self.pair, self.market, self.timeframe, self.o, self.h,
                    self.l, self.c, self.v, self.humanized_timestamp())


OHLCV.init_manager()
