from data.base import BaseModel
from data.order import Order


class StrategyConf(BaseModel):
    collection_name = "strategies"

    def __init__(self, name: str, user_id: str, sub_type: str, conf: dict, key: str,
                 open_on_soft: bool = False, close_on_soft: bool = False, last_op: str = None, fee: float = 0.0, _id=None):
        super().__init__()
        self._id = _id
        self.name = name
        self.user_id = user_id
        self.sub_type = sub_type
        self.conf = conf
        self.key = key
        self.open_on_soft = open_on_soft
        self.close_on_soft = close_on_soft
        self.last_op = last_op
        self.fee = fee

    def serialize(self):
        return {
            "_id": str(self._id),
            "name": self.name,
            "user_id": self.user_id,
            "sub_type": self.sub_type,
            "key": self.key,
            "conf": self.conf,
            "open_on_soft": self.open_on_soft,
            "close_on_soft": self.close_on_soft,
            "fee": self.fee,
            "last_op": self.last_op,
        }

    async def calculate_changes(self):
        if self._id is None:
            return

        orders = await Order.manager.load(strategy_id=self._id)
        s1_change = 0.0
        s2_change = 0.0

        pair = self.conf["pair"]
        s1, s2 = pair.split("/")
        # calc only CLOSED positions
        if len(orders):
            if orders[0].op == orders[-1].op:
                orders.pop()

        for order in orders:
            price = float(order.price)
            amount = float(order.amount)
            if order.op == "buy":
                s1_change += amount * (1 - self.fee / 100.0)
                s2_change -= amount * price
            elif order.op == "sell":
                s1_change -= amount
                s2_change += amount * price * (1 - self.fee / 100.0)

        return s1_change, s2_change

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "Strategy({} {} {} {}, op_soft={}, cl_soft={})"\
            .format(self.name, self.user_id, self.sub_type, self.key, self.open_on_soft, self.close_on_soft)


StrategyConf.init_manager()
