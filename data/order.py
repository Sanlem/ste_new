import time
from datetime import datetime
from data.base import BaseModel


class Order(BaseModel):
    collection_name = "orders"

    def __init__(self, strategy_id: str, order_id: str, market: str, pair: str, op: str, price: float, amount: float,
                 fee: float = 0.0, timestamp: int = None, timestamp_hum: str = None, comment: str = None, _id=None):
        super().__init__()
        self._id = _id
        self.strategy_id = strategy_id
        self.order_id = str(order_id)
        self.market = market
        self.pair = pair
        self.op = op
        self.price = float(price)
        self.amount = float(amount)
        self.fee = fee
        self.comment = comment

        if timestamp is not None:
            self.timestamp = timestamp
            self.timestamp_hum = timestamp_hum
        else:
            self.timestamp = int(time.time())
            ts = datetime.utcfromtimestamp(self.timestamp)
            self.timestamp_hum = ts.strftime("%Y-%m-%d %H:%M:%S")

    def serialize(self):
        return {
            "_id": str(self._id),
            "strategy_id": self.strategy_id,
            "order_id": self.order_id,
            "market": self.market,
            "pair": self.pair,
            "op": self.op,
            "price": self.price,
            "amount": self.amount,
            "fee": self.fee,
            "timestamp": self.timestamp,
            "timestamp_hum": self.timestamp_hum,
            "comment": self.comment,
        }

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "Order({} {} {} {} {} {} {})"\
            .format(self._id, self.market, self.pair, self.op, self.price, self.amount, self.comment)


Order.init_manager()
