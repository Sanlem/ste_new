from datetime import datetime
import time
from data.base import BaseModel


class Ticker(BaseModel):
    collection_name = "tickers"

    def __init__(self, pair, market, ask, bid, last, timestamp=None, _id=None):
        super().__init__()
        self._id = _id
        self.pair = pair
        self.market = market
        self.ask = ask
        self.bid = bid
        self.last = last

        if timestamp is not None:
            self.timestamp = timestamp
        else:
            self.timestamp = int(time.time())

    def serialize(self):
        return {
            "_id": str(self._id),
            "pair": self.pair,
            "market": self.market,
            "ask": self.ask,
            "bid": self.bid,
            "last": self.last,
            "timestamp": self.timestamp,
        }

    def __str__(self):
        ts = datetime.utcfromtimestamp(self.timestamp)
        ts_hum = ts.strftime("%Y-%m-%d %H:%M:%S")
        return "Ticker({}, {}, a={}, b={}, l={}, ts={})"\
            .format(self.pair, self.market, self.ask, self.bid, self.last, ts_hum)


Ticker.init_manager()
