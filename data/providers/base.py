import logging
import asyncio
import sys
from abc import ABC, abstractmethod
from data.base import BaseModel
from data.providers.conf import Conf


class AbstractProvider(ABC):
    def __init__(self, in_queue: asyncio.Queue, conf: dict):
        self.in_queue = in_queue
        self.subscribers = set()
        self.log = logging.getLogger(type(self).__name__)
        self.conf = conf

    @abstractmethod
    def subscribe(self, q: asyncio.Queue, conf: Conf):
        pass

    @abstractmethod
    def unsubscribe(self, q: asyncio.Queue, conf: Conf):
        pass

    @abstractmethod
    async def on_stop(self):
        pass

    @abstractmethod
    async def process_new_object(self, obj: BaseModel):
        pass

    @abstractmethod
    async def load_initial_data(self):
        pass

    @abstractmethod
    async def send_to_subscribers(self, obj: BaseModel = None):
        pass

    @abstractmethod
    async def run(self):
        pass


class BaseProvider(AbstractProvider):
    def subscribe(self, q: asyncio.Queue, conf: Conf):
        self.subscribers.add(q)

    def unsubscribe(self, q: asyncio.Queue, conf: Conf):
        self.subscribers.discard(q)

    async def on_stop(self):
        self.log.warning("Got stop signal!")

    async def process_new_object(self, obj: BaseModel):
        raise NotImplementedError

    async def load_initial_data(self):
        raise NotImplementedError

    async def send_to_subscribers(self, obj: BaseModel = None):
        try:
            if len(self.subscribers) > 0:
                loop = asyncio.get_event_loop()
                for subscriber_queue in self.subscribers:
                    loop.create_task(subscriber_queue.put(obj))
        except Exception:
            self.log.error(f"SendToSubscribers {obj}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def run(self):
        self.log.info(f"Starting {type(self).__name__}...")

        await self.load_initial_data()
        while True:
            try:
                new_object = await self.in_queue.get()
                self.in_queue.task_done()

                if new_object is None:
                    # stop signal
                    await self.on_stop()
                    break

                new_object = await self.process_new_object(new_object)

                if new_object is not None:
                    await self.send_to_subscribers(new_object)
            except Exception:
                self.log.error(f"Run: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
