from typing import Optional
from abc import ABC, abstractmethod


class Conf(ABC):
    @abstractmethod
    def get_key(self):
        pass

    @classmethod
    @abstractmethod
    def deserialize(cls, **kwargs) -> Optional["SignalConf"]:
        pass


class OHLCVConf(Conf):
    def __init__(self, pair: str, market: str, timeframe: str):
        self.pair = pair
        self.market = market
        self.timeframe = timeframe

    @classmethod
    def deserialize(cls, **kwargs) -> Optional["OHLCVConf"]:
        return OHLCVConf(**kwargs)

    def get_ohlcv_key(self):
        return f"{self.pair}:{self.market}:{self.timeframe}"

    def get_key(self):
        return f"{self.pair}:{self.market}:{self.timeframe}"

    def __eq__(self, other):
        return self.market == other.market and self.pair == other.pair \
               and self.timeframe == other.timeframe
