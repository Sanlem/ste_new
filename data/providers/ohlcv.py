from typing import List
import sys
import asyncio
from data.ohlcv import OHLCV
from data.providers.base import BaseProvider
from data.providers.conf import OHLCVConf


class OHLCVProvider(BaseProvider):
    def __init__(self, in_queue: asyncio.Queue, conf: dict):
        super().__init__(in_queue, conf)

        self.ohlcv = {}   # market: pair: timeframe: candles
        self.subscriptions = {}

    def unsubscribe(self, q: asyncio.Queue, conf: OHLCVConf):
        try:
            key = conf.get_key()

            if key in self.subscriptions:
                was_subscriptions_count = len(self.subscriptions[key])
                self.subscriptions[key].discard(q)
                curr_subscr_count = len(self.subscriptions[key])
                self.log.info(f"Removed subscription {key}, was {was_subscriptions_count} subsc, now {curr_subscr_count}")
            else:
                self.log.error(f"No conf {key} in subscription!")
        except Exception:
            self.log.error(f"OhlcvProvider unsubscribe {conf.get_key()}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    def subscribe(self, q: asyncio.Queue, conf: OHLCVConf):
        try:
            key = conf.get_key()

            if key not in self.subscriptions:
                self.subscriptions[key] = set()

            self.subscriptions[key].add(q)
        except Exception:
            self.log.error(f"OhlcvProvider sssssubscribe {conf.get_key()}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    def get_candles(self, market, pair, timeframe) -> List[OHLCV]:
        try:
            conf = OHLCVConf(pair, market, timeframe)
            candles = self.ohlcv[conf.get_key()]
            return candles
        except Exception:
            self.log.error(f"{market} {pair} {timeframe} GetLastCandle: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    def get_last(self, market, pair, timeframe) -> OHLCV:
        try:
            conf = OHLCVConf(pair, market, timeframe)
            candles = self.ohlcv[conf.get_key()]
            if len(candles) > 0:
                return candles[-1]
        except Exception:
            self.log.error(f"{market} {pair} {timeframe} GetLastCandle: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    def update_last(self, market, pair, timeframe, candle: OHLCV):
        try:
            # check
            last = self.get_last(market, pair, timeframe)

            if last.timestamp != candle.timestamp:
                return ValueError(f"Try to update last OHLCV {last} with not last {candle}!")

            conf = OHLCVConf(pair, market, timeframe)
            self.ohlcv[conf.get_key()][-1] = candle
        except Exception:
            self.log.error(f"{market} {pair} {timeframe} UpdateCandle: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    def add_candle(self, market, pair, timeframe, candle: OHLCV):
        try:
            conf = OHLCVConf(pair, market, timeframe)
            self.ohlcv[conf.get_key()].append(candle)
        except Exception:
            self.log.error(f"{market} {pair} {timeframe} addCandle: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    def set_candles(self, market, pair, timeframe, candles: List[OHLCV]):
        try:
            conf = OHLCVConf(pair, market, timeframe)
            self.ohlcv[conf.get_key()] = candles
        except Exception:
            self.log.error(f"{market} {pair} {timeframe} SetCandles: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def update_candle(self, candle: OHLCV):
        try:
            pair = candle.pair
            market = candle.market
            timeframe = candle.timeframe

            conf = OHLCVConf(pair, market, timeframe)

            for old_c in self.ohlcv[conf.get_key()]:
                if old_c.timestamp > 0:
                    pass

        except Exception:
            self.log.error(f"UpdateCandle {candle}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_new_object(self, candle: OHLCV):
        try:
            pair = candle.pair
            market = candle.market
            timeframe = candle.timeframe

            last_candle = self.get_last(market, pair, timeframe)

            if last_candle is None or candle.timestamp > last_candle.timestamp:
                await candle.save()
                self.add_candle(market, pair, timeframe, candle)
                # self.log.info(f"Got new OHLCV: {candle}")
                return None   # not to send this to subscribers
            elif candle.timestamp == last_candle.timestamp:
                last_candle.c = candle.c
                last_candle.h = candle.h
                last_candle.l = candle.l
                last_candle.v = candle.v
                await last_candle.save()

                ll = self.get_last(market, pair, timeframe)

                if ll.c != last_candle.c:
                    self.log.error(f"LL c != last C!")
                if ll.o != last_candle.o:
                    self.log.error(f"LL c != last C!")
                # self.update_last(market, pair, timeframe, candle)
                self.log.info(f"Updated OHLCV: {last_candle}")
                return last_candle
            elif candle.timestamp < last_candle.timestamp:
                # old candle from snapshot
                return {}
            # self.log.error(f"Got {pair} {market} candle ts {candle.timestamp}, last saved ts is {last_candle.timestamp}!")
            # return None
        except Exception:
            self.log.error(f"ProcessNewOHLCV: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def send_to_subscribers(self, obj: OHLCV = None):
        try:
            if not obj:
                return

            conf = OHLCVConf(obj.pair, obj.market, obj.timeframe)
            key = conf.get_key()

            if key in self.subscriptions and key in self.ohlcv:
                last_candles = self.ohlcv[key]
                last_candles = last_candles[-120:]
                loop = asyncio.get_event_loop()
                for subscriber in self.subscriptions[key]:
                    loop.create_task(subscriber.put(last_candles))
        except Exception:
            self.log.error(f"SendToSubscribes: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def load_initial_data(self):
        try:
            for market, market_conf in self.conf.items():
                for pair in market_conf["pairs"]:
                    for timeframe in market_conf["timeframes"]:
                        ohlcv = await OHLCV.manager.load(**{
                            "pair": pair,
                            "market": market,
                            "timeframe": timeframe
                        })

                        self.set_candles(market, pair, timeframe, ohlcv)
                        last_candle = self.get_last(market, pair, timeframe)
                        self.log.info(
                            f"Loaded {len(ohlcv)} candles for {market} {pair} {timeframe}: {[o.humanized_timestamp() for o in ohlcv][:5]}... Last is {last_candle}")

        except Exception:
            self.log.error(f"Ohlcv load: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
