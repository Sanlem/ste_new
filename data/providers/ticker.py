import asyncio
import sys
from data.ticker import Ticker
from data.providers.base import BaseProvider


class TickerProvider(BaseProvider):
    def __init__(self, in_queue: asyncio.Queue, conf: dict):
        super().__init__(in_queue, conf)

        self.tickers = {}

    def get_last(self, market, pair) -> Ticker:
        key = self.get_ticker_key(market, pair)
        return self.tickers.get(key, None)

    @staticmethod
    def get_ticker_key(market: str, pair: str) -> str:
        return f"{market}:{pair}"

    async def process_new_object(self, ticker: Ticker):
        try:
            pair = ticker.pair
            market = ticker.market

            last_ticker = self.get_last(market, pair)

            if last_ticker is None or ticker.timestamp > last_ticker.timestamp:
                await ticker.save()
                self.tickers[self.get_ticker_key(market, pair)] = ticker
                # self.log.info(f"Got new ticker: {ticker}")
                return ticker

            self.log.error(f"Got {pair} {market} ticker ts {ticker.timestamp}, last saved ts is {last_ticker.timestamp}!")
            return None
        except Exception:
            self.log.error(f"ProcessNewTicker: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def load_initial_data(self):
        try:
            for market, pairs in self.conf.items():
                for pair in pairs:
                    ticker = await Ticker.manager.load_one(pair=pair, market=market)
                    key = self.get_ticker_key(market, pair)
                    self.tickers[key] = ticker
                    self.log.info(f"Loaded last ticker {market} {pair} {ticker}")
        except Exception:
            self.log.error(f"LoadInitialData: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
