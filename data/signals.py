from data.base import BaseModel


class Signal(BaseModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
