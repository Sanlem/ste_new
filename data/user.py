import hashlib, binascii, os
from typing import Optional
from data.base import BaseModel, Manager


class UserManager(Manager):
    async def create_user(self, login: str, password: str) -> Optional["User"]:
        # 1. Check if user exists.
        user = await self.load_one(login=login)
        if user is not None:
            raise AttributeError(f"User {login} already exist!")

        hashed_password = self.hash_password(password)
        user = self.model_type(login, hashed_password)
        await user.save()
        return user

    @staticmethod
    def hash_password(password):
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                      salt, 100000)
        pwdhash = binascii.hexlify(pwdhash)
        pwdhash = (salt + pwdhash).decode('ascii')
        return pwdhash


class User(BaseModel):
    collection_name = "users"

    def __init__(self, login: str, password_hash: str, telegram_id: str = None, _id=None):
        super().__init__()
        self.login = login
        self.passwod_hash = password_hash
        self._id = _id
        self.telegram_id = telegram_id

    def serialize(self):
        return {
            "_id": self._id,
            "login": self.login,
            "password_hash": self.passwod_hash,
            "telegram_id": self.telegram_id,
        }

    def check_password(self, password):
        salt = self.passwod_hash[:64]
        stored_password = self.passwod_hash[64:]
        pwdhash = hashlib.pbkdf2_hmac('sha512',
                                      password.encode('utf-8'),
                                      salt.encode('ascii'),
                                      100000)
        pwdhash = binascii.hexlify(pwdhash).decode('ascii')
        return pwdhash == stored_password

    def change_password(self, new_password):
        new_password = self.manager.hash_password(new_password)
        self.passwod_hash = new_password

    def __repr__(self):
        return f"User({self._id}, {self.login}, {self.telegram_id}"


User.init_manager(UserManager)
