from data.base import BaseModel


class NotificationSubscription(BaseModel):
    collection_name = "notificationsub"

    def __init__(self, user_id: str, sub_type: str, conf: dict, send_to: str, key: str, _id=None):
        super().__init__()
        self._id = _id
        self.user_id = user_id
        self.sub_type = sub_type
        self.conf = conf
        self.send_to = send_to
        self.key = key

    def serialize(self):
        return {
            "_id": str(self._id),
            "user_id": self.user_id,
            "sub_type": self.sub_type,
            "send_to": self.send_to,
            "key": self.key,
            "conf": self.conf,
        }

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "NotificationSubscription({} {} {} {})"\
            .format(self._id, self.user_id, self.sub_type, self.conf)


NotificationSubscription.init_manager()
