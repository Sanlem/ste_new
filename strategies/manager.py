import logging
import sys
from asyncio import get_event_loop
from .base import BaseStrategy, StrategyConf
from data.providers import OHLCVProvider, TickerProvider


log = logging.getLogger("strategies.manager")


class StrategiesManager(object):
    def __init__(self, ohlcv_provider: OHLCVProvider, ticker_provider: TickerProvider):
        self.ohlcv_provider = ohlcv_provider
        self.ticker_provider = ticker_provider

        self.loop = get_event_loop()
        self.strategies = {}

    async def add_strategy(self, strategy_conf: StrategyConf):
        try:
            if strategy_conf._id not in self.strategies:
                strategy = BaseStrategy.create_from_conf(strategy_conf, self.ohlcv_provider, self.ticker_provider)
                self.strategies[str(strategy.strategy_conf._id)] = strategy

                self.loop.create_task(strategy.run())
            else:
                log.warning(f"Trying to duplicate strategy {strategy_conf}")
        except Exception:
            log.error(f"addStrategy: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def remove_strategy(self, strategy_id):
        try:
            strategy_id = str(strategy_id)
            strategy = self.strategies.pop(strategy_id, None)

            if strategy is None:
                log.warning(f"Try to remove unexisting strategy {strategy_id}.")
                return

            strategy.should_stop = True
        except Exception:
            log.error(f"RemoveStrategy: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def run(self):
        # just load from db
        log.info("Starting StrategiesManager...")
        try:
            confs = await StrategyConf.manager.load()

            for conf in confs:
                await self.add_strategy(conf)

            log.info(f"Loaded {len(self.strategies.keys())} strategies!")
        except Exception:
            log.error(f"Run: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")


