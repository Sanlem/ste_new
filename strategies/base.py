from abc import ABC, abstractmethod
from asyncio import get_event_loop
from typing import Type, Optional
from asyncio import Queue
import logging
import sys
from data.providers.ohlcv import OHLCVProvider
from data.providers.ticker import TickerProvider
from data.order import Order
from data.strategy import StrategyConf
from signals.providers.base import BaseSignalProvider, BaseSignal
from signals.signal_confs import OHLCVConf
from clients.base import SimulationClient, BaseClient
from web.ws_handler import CH_CONF


log = logging.getLogger("strategies")


class AbstractBaseStrategy(ABC):
    @classmethod
    @abstractmethod
    def create_from_conf(cls, conf: StrategyConf, ohlcv_provider: OHLCVProvider, ticker_provider: TickerProvider):
        pass

    @abstractmethod
    async def run(self):
        pass

    @abstractmethod
    async def process_signal(self, signal: BaseSignal):
        pass

    @abstractmethod
    async def process_order(self, order: Order):
        pass


class BaseStrategy(AbstractBaseStrategy):
    def __init__(self, client: BaseClient,
                 signal_provider_class: Type[BaseSignalProvider], signal_conf: OHLCVConf,
                 ohlcv_provider: OHLCVProvider, ticker_provider: TickerProvider,
                 strategy_conf: StrategyConf):
        self.client = client
        self.in_queue = Queue()
        self.signal_conf = signal_conf
        self.ohlcv_provider = ohlcv_provider
        self.ticker_provider = ticker_provider
        self.signal_provider = signal_provider_class(self.in_queue, self.signal_conf, self.ohlcv_provider)
        self.open_on_soft = strategy_conf.open_on_soft
        self.close_on_soft = strategy_conf.close_on_soft
        self.should_stop = False
        self.last_op = strategy_conf.last_op

        self.strategy_conf = strategy_conf

        self.market = signal_conf.market
        self.pair = signal_conf.pair

        self.log = logging.getLogger(f"Strategy{self.strategy_conf._id}")

    def get_key(self):
        return f"{type(self).__name__}:{self.signal_conf.get_key()}"

    @classmethod
    def create_from_conf(cls, strategy_conf: StrategyConf, ohlcv_provider: OHLCVProvider,
                         ticker_provider: TickerProvider) -> Optional["BaseStrategy"]:
        try:
            sub_type = strategy_conf.sub_type

            conf_class = CH_CONF[sub_type]["conf_class"]
            provider_class = CH_CONF[sub_type]["provider_class"]
            needed_fields = CH_CONF[sub_type]["needed_fields"]

            try:
                conf_dict = {f: strategy_conf.conf[f] for f in needed_fields}
            except KeyError:
                log.error(f"Missing field for {conf_class.__name__}: {strategy_conf.conf}!")
                return None

            conf = conf_class.deserialize(**conf_dict)
            if conf is None:
                log.error(f"{conf_class.__name__} from {conf_dict} is None!")
                return
            client = SimulationClient("dummy", conf.market)

            return cls(client, provider_class, conf, ohlcv_provider, ticker_provider, strategy_conf)
        except Exception:
            log.error(f"CreateStrategyFromConf: {strategy_conf.sub_type} {strategy_conf.key}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def run(self):
        self.log.info(f"Starting {self.get_key()}...")
        loop = get_event_loop()
        loop.create_task(self.signal_provider.run())

        while self.should_stop is False:
            try:
                signal = await self.in_queue.get()
                self.in_queue.task_done()

                if signal is None:
                    self.log.error(f"{self.get_key()} got None signal!")
                    continue

                await self.process_signal(signal)
            except Exception:
                self.log.error(f"{self.get_key()} run: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
        self.signal_provider.should_stop = True
        self.log.info(f"Stopping {self.get_key()}")

    async def get_order_amount(self, pair, op):
        return 1.0

    async def get_order_price(self, pair, op):
        ticker = self.ticker_provider.get_last(self.market, pair)
        if ticker is None:
            return None

        if op == "buy":
            return ticker.ask
        elif op == "sell":
            return ticker.bid
        else:
            self.log.error(f"{self.get_key()} GetOrderPrice {pair} get op {op}!")

    async def process_signal(self, signal: BaseSignal):
        try:
            if signal.signal == "-":
                return

            op_sig = signal.signal

            signals_to_buy = ["BUY", "SOFT BUY"] if self.open_on_soft in [True, "on"] else ["BUY"]
            signals_to_sell = ["SELL", "SOFT SELL"] if self.open_on_soft in [True, "on"] else ["SELL"]

            if self.last_op is None:
                if op_sig in signals_to_buy or op_sig in signals_to_sell:
                    op_sig = op_sig.replace("SOFT ", "")
                    op_sig = op_sig.lower()
                    order_op = op_sig
                else:
                    self.log.info(f"No last op, signal is {op_sig}")
                    op_sig = op_sig.replace("SOFT ", "")
                    op_sig = op_sig.lower()
                    order_op = op_sig

            else:
                signals_to_buy = ["BUY", "SOFT BUY"] if self.close_on_soft in [True, "on"] else ["BUY"]
                signals_to_sell = ["SELL", "SOFT SELL"] if self.close_on_soft in [True, "on"] else ["SELL"]
                if self.last_op == "buy" and op_sig in signals_to_sell:
                    order_op = "sell"
                elif self.last_op == "sell" and op_sig in signals_to_buy:
                    order_op = "buy"
                else:
                    self.log.info(f"Wont close last {self.last_op} position, signal is {op_sig}")
                    return

            price = await self.get_order_price(self.pair, order_op)
            if price is None:
                self.log.error(f"{self.get_key()} won't {order_op}: price is None!")
                return

            amount = await self.get_order_amount(self.pair, order_op)
            if price is None:
                self.log.error(f"{self.get_key()} won't {order_op}: amount is None!")
                return

            order = await self.client.place_order(self.pair, order_op, price, amount)

            if order is not None:
                order.comment = signal.comment
                return await self.process_order(order)
            else:
                self.log.error(f"{self.get_key()}{order_op} {price} {amount} order is None!")
        except Exception:
            self.log.error(f"{self.get_key()} ProcessSignal: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_order(self, order: Order):
        try:
            self.last_op = order.op
            order.strategy_id = self.strategy_conf._id
            await order.save()
            self.log.info(f"Done order: {order}")
            self.strategy_conf.last_op = order.op
            await self.strategy_conf.save()
        except Exception:
            self.log.error(f"{self.get_key()} ProcessOrder: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
