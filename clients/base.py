from abc import ABC, abstractmethod
from typing import Optional
from data.order import Order


class AbstractBaseClient(ABC):
    @abstractmethod
    async def place_order(self, pair: str, op: str, price: float, amount: float) -> Optional[Order]:
        pass

    @abstractmethod
    async def get_balance(self) -> Optional[dict]:
        pass


class BaseClient(AbstractBaseClient):
    def __init__(self, base_url: str, market: str, user: str = None, key: str = None, secret: str = None):
        self.base_url = base_url
        self.market = market
        self.user = user
        self.key = key
        self.secret = secret

    async def place_order(self, pair: str, op: str, price: float, amount: float) -> Optional[Order]:
        raise NotImplementedError

    async def get_balance(self) -> Optional[dict]:
        raise NotImplementedError


class SimulationClient(BaseClient):
    def __init__(self, base_url: str, market: str, user: str = None, key: str = None, secret: str = None,
                 balance_s1: float = 0, balance_s2: float = 0):
        super().__init__(base_url, market, user, key, secret)
        self.balance_s1 = balance_s1
        self.balance_s2 = balance_s2
        self.last_id = 0

    async def place_order(self, pair: str, op: str, price: float, amount: float) -> Optional[Order]:
        order_id = str(self.last_id + 1)
        self.last_id += 1
        order = Order("None", order_id, self.market, pair, op, price, amount)
        await order.save()
        return order

    async def get_balance(self) -> Optional[dict]:
        return {}
