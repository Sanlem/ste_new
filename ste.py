import logging.config
# import config
import asyncio
from aiohttp import web
from conf import config, logging_config, templates_dir
from db import db, mongo, redis_pool
from aiohttp_session import setup as setup_session
from aiohttp_session.redis_storage import RedisStorage
from aiohttp_jinja2 import setup as setup_templates
from jinja2 import FileSystemLoader
from aiohttp_flashbag import flashbag_middleware
from data.ticker import Ticker
from data.providers import TickerProvider, OHLCVProvider
from web import init_routes
import ws
from notifications.telegram_bot import TelegramBot
from notifications.manager import NotificationsManager
from strategies.manager import StrategiesManager


logging.config.dictConfig(logging_config)
log = logging.getLogger(__name__)


def init_app():
    app = web.Application()
    app["config"] = config

    app["mongo"] = mongo
    app["db"] = db
    app["ws_connections"] = set()

    init_routes(app)

    setup_templates(app, loader=FileSystemLoader(str(templates_dir)))

    ticker_queue = asyncio.Queue()
    ticker_provider = TickerProvider(ticker_queue, config["available"]["ticker"])
    app["ticker_queue"] = ticker_queue
    app["ticker_provider"] = ticker_provider

    ohlcv_queue = asyncio.Queue()
    ohlcv_provider = OHLCVProvider(ohlcv_queue, config["available"]["ohlcv"])
    app["ohlcv_queue"] = ohlcv_queue
    app["ohlcv_provider"] = ohlcv_provider

    app["ws_clients"] = {}
    for market, market_conf in config["ws_subscriptions"].items():
        market_ws_class = ws.get_ws_class(market)

        if market_ws_class is None:
            log.error("Not found ws client class for {}!".format(market))
            continue

        app["ws_clients"][market] = market_ws_class(market_conf["url"], market_conf["conf"],
                                                    ticker_queue=ticker_queue, ohlcv_queue=ohlcv_queue)

    notifications_conf = config["notifications"]
    app["notifications_manager"] = NotificationsManager(ohlcv_provider, notifications_conf)

    tg_conf = notifications_conf["telegram"]
    app["telegram_bot"] = TelegramBot(tg_conf["url"], tg_conf["token"])

    strategies_manager = StrategiesManager(ohlcv_provider, ticker_provider)
    app["strategies_manager"] = strategies_manager

    app.on_startup.append(init_sessions)
    app.on_startup.append(run_data_providers)
    app.on_startup.append(run_data_ws)
    app.on_startup.append(run_misc)
    return app


async def init_sessions(app):
    pool = await redis_pool
    app["redis_pool"] = pool

    cookie_storage = RedisStorage(pool, cookie_name="STE_SESSION")
    setup_session(app, cookie_storage)

    app.middlewares.append(flashbag_middleware)
    app["cookie_storage"] = cookie_storage
    log.info(f"Initialised cookie storage: {cookie_storage}")


async def run_data_providers(app):
    # from db import mongo
    # await mongo.drop_database("ste_db")
    #
    # await asyncio.sleep(100)
    app["data_providers_tasks"] = []
    app["data_providers_tasks"].append(app.loop.create_task(app["ticker_provider"].run()))
    app["data_providers_tasks"].append(app.loop.create_task(app["ohlcv_provider"].run()))


async def run_misc(app):
    # db.drop_collection("notificationsub")
    # print("11111")
    tg_bot = app["telegram_bot"]
    notifications_manager = app["notifications_manager"]
    strategies_manager = app["strategies_manager"]

    app["misc_tasks"] = []
    app["misc_tasks"].append(app.loop.create_task(tg_bot.run()))
    app["misc_tasks"].append(app.loop.create_task(notifications_manager.run()))
    app["misc_tasks"].append(app.loop.create_task(strategies_manager.run()))


async def run_data_ws(app):
    # from data.ohlcv import OHLCV
    # candles = await OHLCV.manager.load(market="kraken")
    # for c in candles:
    #     await c.delete()
    #
    # print("STOOOOOOOOOOOOOpp")
    app["data_ws_tasks"] = []

    for market, ws in app["ws_clients"].items():
        app["data_ws_tasks"].append(app.loop.create_task(ws.run()))


async def test_models(app):
    t = Ticker("BTC/USD", "bitfinex", 100, 98, 99)
    await t.save()
    print(t._id)

    tickers = await Ticker.manager.load()
    print(tickers)


if __name__ == "__main__":
    app = init_app()
    host = app["config"]["host"]
    port = app["config"]["port"]

    log.info("Run at {}:{}".format(host, port))
    web.run_app(app, host=host, port=port)
