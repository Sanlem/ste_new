from conf import config
import motor.motor_asyncio
import aioredis


mongo_host = config["mongodb"]["host"]
mongo_port = config["mongodb"]["port"]

mongo = motor.motor_asyncio.AsyncIOMotorClient(mongo_host, mongo_port)
db = mongo["ste_db"]
# db = mongo["ste_test_db"]

redis_host = config["redis"]["host"]
redis_port = config["redis"]["port"]
redis_pool = aioredis.create_pool((redis_host, redis_port))
