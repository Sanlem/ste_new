import asyncio
import pytest
import logging
from data.ticker import Ticker


logging.basicConfig()
log = logging.getLogger("models_test")


async def test():
    import db
    await db.mongo.drop_database("ste_test_db")

    tickers = await Ticker.manager.load()
    assert len(tickers) == 0, "Expected {} tickers, got {}".format(0, len(tickers))

    new_t = Ticker("BTC/USD", "cex", 10, 9, 9.5)
    await new_t.save()

    tickers = await Ticker.manager.load()
    assert len(tickers) == 1

    t_ids = [t._id for t in tickers]

    assert new_t._id in t_ids, "Expected new ticker id {} to be in {}".format(new_t._id, t_ids)

    new_t.ask = 15
    await new_t.save()

    tickers = await Ticker.manager.load()
    assert len(tickers) == 1

    assert tickers[0].ask == 15

    new_t2 = Ticker("BTC/USD", "cex", 10, 8, 9.5)
    await new_t2.save()

    tickers = await Ticker.manager.load()
    assert len(tickers) == 2

    t_ids = [t._id for t in tickers]

    assert new_t._id in t_ids, "Expected new ticker id {} to be in {}".format(new_t._id, t_ids)
    assert new_t2._id in t_ids, "Expected new ticker 2 id {} to be in {}".format(new_t2._id, t_ids)

    await new_t.delete()

    tickers = await Ticker.manager.load()
    assert len(tickers) == 1

    t_ids = [t._id for t in tickers]

    assert new_t._id not in t_ids, "Expected new ticker id {} not to be in {}".format(new_t._id, t_ids)
    assert new_t2._id in t_ids, "Expected new ticker 2 id {} to be in {}".format(new_t2._id, t_ids)

    t2_copy = await Ticker.load_by_id(new_t2._id)

    assert t2_copy._id == new_t2._id


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test())
