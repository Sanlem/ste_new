import asyncio
import json
import sys
from typing import List
from ws.base import BaseWS
from data.ticker import Ticker
from data.ohlcv import OHLCV


class BitfinexWS(BaseWS):
    pairs_map = {}

    def __init__(self, url: str, conf: dict, ticker_queue: asyncio.Queue = None,
                 ohlcv_queue: asyncio.Queue = None):
        super().__init__("bitfimex", url, conf)

        pairs = set(conf["ticker"])

        pairs.update(conf["ohlcv"]["pairs"])

        BitfinexWS.pairs_map = {pair.replace("/", ""): pair for pair in pairs}
        self.subscriptions = {}

        self.ticker_queue = ticker_queue
        self.ohlcv_queue = ohlcv_queue

    async def perform_subscriptions(self):
        try:
            await super().perform_subscriptions()
            subscriptions_msg_list = []

            for channel, channel_conf in self.conf.items():
                if channel == "ticker":
                    for pair in channel_conf:
                        bfx_pair = self.convert_pair_to_market_format(pair)
                        msg = {
                            "event": "subscribe",
                            "channel": "ticker",
                            "pair": "t{}".format(bfx_pair)
                        }

                        subscriptions_msg_list.append(msg)

                if channel == "ohlcv":
                    for pair in channel_conf["pairs"]:
                        for timeframe in channel_conf["timeframes"]:
                            bfx_pair = self.convert_pair_to_market_format(pair)
                            msg = {
                                "event": "subscribe",
                                "channel": "candles",
                                "key": f"trade:{timeframe}:t{bfx_pair}"
                            }

                            subscriptions_msg_list.append(msg)

            # subscriptions_msg_list.append({
            #     "event": "subscribe",
            #     "channel": "candles",
            #     "key": "trade:1m:tBTCUSD"
            # })

            for msg in subscriptions_msg_list:
                await self.send_message(msg)
        except Exception:
            self.log.error(f"PerformSubscriptions: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_message(self, msg):
        try:
            # msg_json = msg.json()
            # print(f"Got message {")
            msg_json = json.loads(msg.data)

            if "event" in msg_json and msg_json["event"] == "subscribed":
                return await self.process_subscribe(msg_json)
            elif isinstance(msg_json, list):
                if msg_json[1] == "hb":
                    return

                chanId = msg_json[0]
                key = self.subscriptions.get(chanId, None)

                if key is None:
                    self.log.error("Unexpected subscription chanId: {}".format(chanId))
                    return

                pair = self.get_pair_from_subscription_key(key)
                channel = self.get_channel_from_subscription_key(key)

                if channel == "ticker":
                    await self.process_ticker(pair, msg_json)
                elif "candles" in channel:
                    # print(f"Ohlcv {pair}: {msg}")
                    await self.process_candles(pair, channel, msg_json)

            # self.log.info("Got message: {}".format(msg_json))

        except Exception:
            self.log.error(f"ProcessMessage: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_candles(self, pair, channel, msg):
        try:
            if len(msg[1]) == 0:
                self.log.warning(f"Empty ohlcv for {pair} {channel}")
                return
            # self.log.info(f"Process ohlcv {pair} {channel}: {msg}")

            timeframe = channel.split(":")[1]

            # snapshot or update
            candles = msg[1]

            if type(candles[0]) in (int, float):
                # upd
                candles = [candles]
                # for one processing format

            processed_candles = []
            loop = asyncio.get_event_loop()
            for candle in reversed(candles):
                ms_ts = int(candle[0])
                ts = int(ms_ts / 1000)
                _open = float(candle[1])
                close = float(candle[2])
                high = float(candle[3])
                low = float(candle[4])
                volume = float(candle[5])

                candle_obj = OHLCV(pair, "bitfinex", timeframe, _open,
                                   high, low, close, volume, ts)
                processed_candles.append(candle_obj)
                loop.create_task(self.send_ohlcv(candle_obj))

                # self.log.info(f"New ohlcv: {candle_obj}.")

        except Exception:
            self.log.error(f"{pair} ProcessCandles: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_ticker(self, pair, msg):
        try:
            # self.log.info(f"Process ticker {msg}")
            ticker = msg[1]

            ask = ticker[0]
            bid = ticker[2]
            last = ticker[6]

            ticker_object = Ticker(pair, "bitfinex", ask, bid, last)
            loop = asyncio.get_event_loop()

            loop.create_task(self.send_ticker(ticker_object))
        except Exception:
            self.log.error(f"{pair} ProcessTicker: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def send_ticker(self, ticker: Ticker):
        try:
            # print(f"Send ticker {ticker}")
            if self.ticker_queue is not None:
                await self.ticker_queue.put(ticker)
        except Exception:
            self.log.error(f"SendTicker {ticker}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}.")

    async def send_ohlcv(self, candle:OHLCV):
        try:
            if self.ohlcv_queue is not None:
                await self.ohlcv_queue.put(candle)
        except Exception:
            self.log.error(f"SendCandle {candle}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}.")

    async def process_subscribe(self, msg):
        try:
            # print(msg)
            channel = msg["channel"]
            channel_id = msg["chanId"]

            if channel == "ticker":
                bfx_pair = msg["pair"]
                pair = self.convert_pair_from_market_format(bfx_pair)
                key = self.get_subscription_key(pair, channel)
            elif channel == "candles":
                ohlcv_subscr_key = msg["key"]

                splitted = ohlcv_subscr_key.split(":")

                subscr_pair = splitted[2]
                timeframe = splitted[1]

                bfx_pair = subscr_pair.replace("t", "")

                pair = self.convert_pair_from_market_format(bfx_pair)

                ch = f"{channel}:{timeframe}"
                key = self.get_subscription_key(pair, ch)
            else:
                self.log.error(f"Unexpected channel subscription: {channel}!")
                return

            self.subscriptions[channel_id] = key
            self.log.info("Successfully subscribed to {}".format(key))
        except Exception as e:
            self.log.error(f"ProcessSubscribe: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def send_message(self, msg, json=True):
        # self.log.info("Gonna send {}".format(msg))

        if json is True:
            return await self.ws.send_json(msg)

    @staticmethod
    def get_subscription_key(pair, channel):
        return "{}_{}".format(pair, channel)

    @staticmethod
    def get_pair_from_subscription_key(key):
        pair = key.split("_")[0]
        # pairs in subscriptions keys are already converted.
        # pair = BitfinexWS.convert_pair_from_market_format(pair)
        return pair

    @staticmethod
    def get_channel_from_subscription_key(key):
        chan = key.split("_")[1]
        return chan

    @staticmethod
    def convert_pair_to_market_format(pair):
        pair = pair.replace("DASH", "DSH")
        pair = pair.replace("BCH", "BAB")
        pair = pair.replace("/", "")
        return pair

    @staticmethod
    def convert_pair_from_market_format(pair):
        try:
            return BitfinexWS.pairs_map[pair]
        except KeyError:
            raise KeyError(f"{BitfinexWS.pairs_map} {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
