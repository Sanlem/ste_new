from abc import ABC, abstractmethod
import logging
import aiohttp


class AbstractBaseWS(ABC):
    @abstractmethod
    async def process_message(self, msg):
        pass

    @abstractmethod
    async def perform_subscriptions(self):
        pass

    @abstractmethod
    async def send_message(self, msg):
        pass

    @staticmethod
    @abstractmethod
    def get_pong_msg(self):
        pass

    @staticmethod
    @abstractmethod
    def convert_pair_to_market_format(pair):
        pass

    @staticmethod
    @abstractmethod
    def convert_pair_from_market_format(pair):
        pass


class BaseWS(AbstractBaseWS):
    def __init__(self, market, url, conf):
        self.market = market
        self.url = url
        self.conf = conf
        self.log = logging.getLogger("{}WS".format(market.upper()))
        self.stop = False
        self.ws = None

    async def run(self):
        session = aiohttp.ClientSession()
        while self.stop is False:

            async with session.ws_connect(self.url) as ws:
                self.log.info(f"Opened ws connection to {self.url}")
                self.ws = ws

                await self.perform_subscriptions()
                async for msg in ws:
                    if msg.type == aiohttp.WSMsgType.CLOSED:
                        self.log.warning("Closed ws to {}.".format(self.url))
                    elif msg.type == aiohttp.WSMsgType.ERROR:
                        self.log.error("{}".format(msg))
                    elif msg.type == aiohttp.WSMsgType.TEXT:
                        await self.process_message(msg)
                    else:
                        self.log.warning(f"Got msg {msg.type}: {msg}")

    async def process_message(self, msg):
        self.log.info("Got message: {}".format(msg))

    async def perform_subscriptions(self):
        self.log.info("Gonna perform subscriptions... {}".format(self.conf))

    async def send_message(self, msg, json=True):
        pass

    @staticmethod
    def convert_pair_to_market_format(pair):
        pass

    @staticmethod
    def convert_pair_from_market_format(pair):
        pass

    @staticmethod
    def get_pong_msg(self):
        pass
