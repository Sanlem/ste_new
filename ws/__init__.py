from .bitfinex import BitfinexWS
from .kraken import KrakenWS


classes_map = {
    "bitfinex": BitfinexWS,
    "kraken": KrakenWS,
}


def get_ws_class(market):
    return classes_map.get(market, None)
