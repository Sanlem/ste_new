import asyncio
import json
import sys
from typing import List
from ws.base import BaseWS
from data.ticker import Ticker
from data.ohlcv import OHLCV


timeframe_to_kraken = {
    "1m": 1,
    "15m": 15,
    "30m": 30,
    "1h": 60,
    "4h": 240,
    "1d": 1440
}

kraken_timeframe_to_common = {
    1: "1m",
    15: "15m",
    30: "30m",
    60: "1h",
    240: "4h",
    1440: "1d",
}


class KrakenWS(BaseWS):
    pairs_map = {}

    def __init__(self, url: str, conf: dict, ticker_queue: asyncio.Queue = None,
                 ohlcv_queue: asyncio.Queue = None):
        super().__init__("kraken", url, conf)

        pairs = set(conf["ticker"])

        pairs.update(conf["ohlcv"]["pairs"])

        KrakenWS.pairs_map = {self.convert_pair_to_market_format(pair): pair for pair in pairs}
        self.subscriptions = {}

        self.ticker_queue = ticker_queue
        self.ohlcv_queue = ohlcv_queue

    async def perform_subscriptions(self):
        try:
            await super().perform_subscriptions()
            subscriptions_msg_list = []

            for channel, channel_conf in self.conf.items():
                if channel == "ticker":
                    msg = {
                        "event": "subscribe",
                        "pair": [self.convert_pair_to_market_format(pair) for pair in channel_conf],
                        "subscription": {
                            "name": "ticker",
                        }
                    }
                    subscriptions_msg_list.append(msg)
                elif channel == "ohlcv":
                    for timeframe in channel_conf["timeframes"]:
                        if timeframe not in timeframe_to_kraken:
                            self.log.error(f"Invalid timeframe for Kraken: {timeframe}")
                            continue
                        msg = {
                            "event": "subscribe",
                            "pair": [self.convert_pair_to_market_format(pair) for pair in channel_conf["pairs"]],
                            "subscription": {
                                "name": "ohlc",
                                "interval": timeframe_to_kraken[timeframe]
                            }
                        }
                        subscriptions_msg_list.append(msg)

            for msg in subscriptions_msg_list:
                await self.send_message(msg)
        except Exception:
            self.log.error(f"PerformSubscriptions: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_message(self, msg):
        try:
            # msg_json = msg.json()
            msg_json = json.loads(msg.data)
            # print(f"Got message {msg_json}")
            if isinstance(msg_json, list):
                channel_id = msg_json[0]
                if channel_id not in self.subscriptions:
                    self.log.error(f"Gon unknown channel: {channel_id}")
                    return

                key = self.subscriptions[channel_id]
                pair = self.get_pair_from_subscription_key(key)
                channel = self.get_channel_from_subscription_key(key)

                if channel == "ticker":
                    return await self.process_ticker(pair, msg_json)
                elif "ohlc" in channel:
                    return await self.process_candles(pair, channel, msg_json)
                else:
                    self.log.error(f"No func to process {channel}!")
                return

            event = msg_json.get("event", None)
            if event == "subscriptionStatus":
                await self.process_subscribe(msg_json)
            elif event == "heartbeat":
                pass
            else:
                self.log.warning(f"Unknown message: {msg_json}")

        except Exception:
            self.log.error(f"ProcessMessage: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_candles(self, pair, channel, msg):
        try:
            timeframe = channel.replace("ohlcv", "")
            data = msg[1]

            end_ts_float = float(data[1])

            timestamp = int(end_ts_float) - timeframe_to_kraken[timeframe] * 60
            _open = float(data[2])
            high = float(data[3])
            low = float(data[4])
            close = float(data[5])
            volume = float(data[7])

            candle = OHLCV(pair, "kraken", timeframe, _open, high, low, close, volume, timestamp)

            loop = asyncio.get_event_loop()
            loop.create_task(self.send_ohlcv(candle))
        except Exception:
            self.log.error(f"{pair} {channel} ProcessCandles {msg}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_ticker(self, pair, msg):
        try:
            # self.log.info(f"Process ticker {msg}")
            data = msg[1]
            ask = float(data["a"][0])
            bid = float(data["b"][0])
            last = float(data["c"][0])
            ticker = Ticker(pair, "kraken", ask, bid, last)

            loop = asyncio.get_event_loop()
            loop.create_task(self.send_ticker(ticker))
        except Exception:
            self.log.error(f"{pair} ProcessTicker: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def send_ticker(self, ticker: Ticker):
        try:
            # print(f"Send ticker {ticker}")
            if self.ticker_queue is not None:
                await self.ticker_queue.put(ticker)
        except Exception:
            self.log.error(f"SendTicker {ticker}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}.")

    async def send_ohlcv(self, candle: OHLCV):
        try:
            # print(f"Send OHLCV {candle}")
            if self.ohlcv_queue is not None:
                await self.ohlcv_queue.put(candle)
                # pass
            else:
                self.log.warning(f"Nowhere to send OHLCV!")
        except Exception:
            self.log.error(f"SendCandle {candle}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}.")

    async def process_subscribe(self, msg):
        try:
            if msg["event"] != "subscriptionStatus":
                return

            status = msg["status"]
            if status == "error":
                self.log.error(f"Kraken subscribe: {msg}")
                return

            name = msg["subscription"]["name"]
            channel = msg["channelName"]
            channel_id = msg["channelID"]
            kr_pair = msg["pair"]
            pair = self.convert_pair_from_market_format(kr_pair)

            if pair is None:
                self.log.error(f"Failed to convert Kraken pair: {kr_pair}")
                return

            if name == "ohlc":
                kraken_tf = int(channel.split("-")[1])
                if kraken_tf not in kraken_timeframe_to_common:
                    self.log.error(f"Got unknown ohlcv tf from Kraken: {kraken_tf}")
                    return

                timeframe = kraken_timeframe_to_common[kraken_tf]

                key = self.get_subscription_key(pair, f"ohlcv{timeframe}")
            elif name == "ticker":
                key = self.get_subscription_key(pair, "ticker")
            else:
                self.log.warning(f"Unexpected Kraken channel sub: {name}: {msg}")
                return

            self.subscriptions[channel_id] = key
            self.log.info("Successfully subscribed to {}".format(key))
        except Exception as e:
            self.log.error(f"ProcessSubscribe: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def send_message(self, msg, json=True):
        # self.log.info("Gonna send {}".format(msg))

        if json is True:
            return await self.ws.send_json(msg)

    @staticmethod
    def get_subscription_key(pair, channel):
        return "{}_{}".format(pair, channel)

    @staticmethod
    def get_pair_from_subscription_key(key):
        pair = key.split("_")[0]
        # pairs in subscriptions keys are already converted.
        # pair = BitfinexWS.convert_pair_from_market_format(pair)
        return pair

    @staticmethod
    def get_channel_from_subscription_key(key):
        chan = key.split("_")[1]
        return chan

    @staticmethod
    def convert_pair_to_market_format(pair):
        pair = pair.replace("BTC", "XBT")
        return pair

    @staticmethod
    def convert_pair_from_market_format(pair):
        try:
            return KrakenWS.pairs_map[pair]
        except KeyError:
            raise KeyError(f"{KrakenWS.pairs_map} {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
