from abc import ABC, abstractmethod
from typing import Optional
from data.providers.conf import Conf, OHLCVConf


class BollingerBandsConf(OHLCVConf):
    def __init__(self, pair: str, market: str, timeframe: str, period: int, multiplier: int):
        super().__init__(pair, market, timeframe)
        self.period = int(period)
        self.multiplier = int(multiplier)

    @classmethod
    def deserialize(cls, **kwargs) -> Optional["BollingerBandsConf"]:
        try:
            pair = kwargs["pair"]
            market = kwargs["market"]
            timeframe = kwargs["timeframe"]
            period = kwargs["period"]
            multiplier = kwargs["multiplier"]
        except KeyError:
            return None

        return BollingerBandsConf(pair, market, timeframe, period, multiplier)

    def get_key(self):
        ohlcv_key = super().get_key()
        return f"bollingerbands:{ohlcv_key}:{self.period}:{self.multiplier}"


class StohasticConf(OHLCVConf):
    def __init__(self, pair: str, market: str, timeframe: str, period: int, zone_delta: int = 20, d_period: int = 3):
        super().__init__(pair, market, timeframe)
        self.period = int(period)
        self.d_period = int(d_period)
        self.zone_delta = int(zone_delta)

    @classmethod
    def deserialize(cls, **kwargs) -> Optional["StohasticConf"]:
        try:
            pair = kwargs["pair"]
            market = kwargs["market"]
            timeframe = kwargs["timeframe"]
            period = int(kwargs["period"])
            zone_delta = int(kwargs["zone_delta"])
        except KeyError:
            return None

        return StohasticConf(pair, market, timeframe, period, zone_delta)

    def get_key(self):
        ohlcv_key = super().get_key()
        return f"stohastic:{ohlcv_key}:{self.period}:{self.zone_delta}"


class RSIConf(OHLCVConf):
    def __init__(self, pair: str, market: str, timeframe: str, period: int, zone_delta: int = 20):
        super().__init__(pair, market, timeframe)
        self.period = int(period)
        self.zone_delta = int(zone_delta)

    @classmethod
    def deserialize(cls, **kwargs) -> Optional["RSIConf"]:
        try:
            pair = kwargs["pair"]
            market = kwargs["market"]
            timeframe = kwargs["timeframe"]
            period = int(kwargs["period"])
            zone_delta = int(kwargs["zone_delta"])
        except KeyError:
            return None

        return RSIConf(pair, market, timeframe, period, zone_delta)

    def get_key(self):
        ohlcv_key = super().get_key()
        return f"rsi:{ohlcv_key}:{self.period}:{self.zone_delta}"


class SmaCrossConf(OHLCVConf):
    def __init__(self, pair: str, market: str, timeframe: str, period1: int, period2: int):
        super().__init__(pair, market, timeframe)
        self.period_fast = min(int(period1), int(period2))
        self.period_slow = max(int(period1), int(period2))

    @classmethod
    def deserialize(cls, **kwargs) -> Optional["SmaCrossConf"]:
        try:
            pair = kwargs["pair"]
            market = kwargs["market"]
            timeframe = kwargs["timeframe"]
            period1 = kwargs["period1"]
            period2 = kwargs["period2"]
        except KeyError:
            return None

        return SmaCrossConf(pair, market, timeframe, period1, period2)

    def get_key(self):
        ohlcv_key = super().get_key()
        return f"sma:{ohlcv_key}:{self.period_fast}:{self.period_slow}"


class Signal(ABC):
    @classmethod
    @abstractmethod
    def process(cls, conf: Conf, data) -> Optional["Signal"]:
        pass

    @abstractmethod
    def serialize(self):
        pass


class BollingerBands(Signal):
    @classmethod
    def process(cls, conf: Conf, data) -> Optional[Signal]:
        pass

    @abstractmethod
    def serialize(self):
        pass
