from asyncio import Queue
from data.providers import OHLCVProvider
from signals.providers.base import BaseSignalProvider
from signals.signals import BollingerBandsSignal, BollingerBandsConf


class BollingerBandsProvider(BaseSignalProvider):
    signal_type = BollingerBandsSignal
    subscription_type = "bbands"

    def __init__(self, q: Queue, conf: BollingerBandsConf, ohlcv_provider: OHLCVProvider):
        super().__init__(q, conf, ohlcv_provider)
