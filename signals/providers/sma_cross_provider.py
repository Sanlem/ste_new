from asyncio import Queue
from data.providers import OHLCVProvider
from signals.providers.base import BaseSignalProvider
from signals.signals import SmaCrossConf, SmaCrossSignal


class SmaCrossProvider(BaseSignalProvider):
    signal_type = SmaCrossSignal
    subscription_type = "sma"

    def __init__(self, q: Queue, conf: SmaCrossConf, ohlcv_provider: OHLCVProvider):
        super().__init__(q, conf, ohlcv_provider)
