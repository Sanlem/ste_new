from asyncio import Queue
from data.providers import OHLCVProvider
from signals.providers.base import BaseSignalProvider
from signals.signals import RSISignal, RSIConf


class RSIProvider(BaseSignalProvider):
    signal_type = RSISignal
    subscription_type = "rsi"

    def __init__(self, q: Queue, conf: RSIConf, ohlcv_provider: OHLCVProvider):
        super().__init__(q, conf, ohlcv_provider)
