from asyncio import Queue
from data.providers import OHLCVProvider
from signals.providers.base import BaseSignalProvider
from signals.signals import StohasticConf, StochasticSignal


class StochasticProvider(BaseSignalProvider):
    signal_type = StochasticSignal
    subscription_type = "stoch"

    def __init__(self, q: Queue, conf: StohasticConf, ohlcv_provider: OHLCVProvider):
        super().__init__(q, conf, ohlcv_provider)
