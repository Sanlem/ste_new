from abc import ABC, abstractmethod
from asyncio import Queue, get_event_loop
import logging
import sys
from typing import List, Optional
from data.ohlcv import OHLCV
from data.providers import OHLCVProvider
from signals.signals import BaseSignal
from data.providers.conf import Conf, OHLCVConf


class AbstractBaseSignalProvider(ABC):
    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def send_to_subscriber(self, signal: BaseSignal):
        pass


class BaseSignalProvider(AbstractBaseSignalProvider):
    signal_type = BaseSignal
    subscription_type = None

    def __init__(self, q: Queue, conf: OHLCVConf, ohlcv_provider: OHLCVProvider):
        self.ohlcv_provider = ohlcv_provider
        self.in_queue = Queue()
        self.only_ohlcv_conf = OHLCVConf(conf.pair, conf.market, conf.timeframe)
        self.ohlcv_provider.subscribe(self.in_queue, self.only_ohlcv_conf)
        self.conf = conf
        self.log = logging.getLogger(type(self).__name__)
        self.out_queue = q
        self.loop = get_event_loop()
        self.should_stop = False

    def stop(self):
        self.log.warning(f"Received stop desire.")
        self.should_stop = True

    async def process_new_data(self, candles: List[OHLCV]) -> (Optional[BaseSignal], Optional[Conf]):
        try:
            # 1. Check pair, market and timeframe to know how to process that
            market = candles[-1].market
            pair = candles[-1].pair
            timeframe = candles[-1].timeframe

            ohlcv_conf = OHLCVConf.deserialize(**{
                "pair": pair,
                "market": market,
                "timeframe": timeframe
            })

            if ohlcv_conf != self.conf:
                self.log.error(f"{type(self)} got {ohlcv_conf.get_key()} OHLCV instead of {self.conf.get_key()}")
                return

            signal = self.signal_type.process(self.conf, candles)
            return signal
        except Exception:
            self.log.error(f"{type(self)} process new data: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def send_to_subscriber(self, signal: BaseSignal):
        try:
            # self.log.info(f"{str(type(signal))}: {signal.serialize()}")
            self.loop.create_task(self.out_queue.put(signal))
        except Exception:
            self.log.error(f"{type(self)} send to subscribers {self.conf.get_key()}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def run(self):
        self.log.info(f"Starting {type(self).__name__}...")

        while self.should_stop is False:
            try:
                candles = await self.in_queue.get()
                self.in_queue.task_done()

                if candles is None or self.should_stop is True:
                    # stop signal
                    self.log.info(f"Unsubscribe from {self.only_ohlcv_conf.get_key()}")
                    self.ohlcv_provider.unsubscribe(self.in_queue, self.only_ohlcv_conf)
                else:
                    signal = await self.process_new_data(candles)

                    if signal is not None:
                        await self.send_to_subscriber(signal)
            except Exception:
                self.log.error(f"Run: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
        # Stopping
        self.log.info(f"Stopping {type(self).__name__}")
        # self.out_queue.put_nowait(None)
