from typing import Optional, List, Tuple
from abc import ABC, abstractmethod
from data.ohlcv import OHLCV
from signals.signal_confs import Conf, BollingerBandsConf, StohasticConf, SmaCrossConf, RSIConf
from signals import indicators
import time
import logging


log = logging.getLogger("Signals")


class Signal(ABC):
    @classmethod
    @abstractmethod
    def process(cls, conf: Conf, data) -> Optional["Signal"]:
        pass

    @abstractmethod
    def serialize(self):
        pass


class BaseSignal(Signal):
    def __init__(self, signal: str, comment: str):
        self.signal = signal
        self.comment = comment

    last_meaningful_signal = {}
    last_meaningful_comment = {}

    @classmethod
    def process(cls, conf: Conf, data) -> Optional["BaseSignal"]:
        raise NotImplementedError

    def serialize(self):
        raise NotImplementedError


class BollingerBandsSignal(BaseSignal):
    def __init__(self, top: float, middle: float, bot: float, last: float,
                 signal: str, comment: str, key: str, timestamp: int = None):
        super().__init__(signal, comment)
        self.top = top
        self.bot = bot
        self.middle = middle
        self.last = last
        self.timestamp = timestamp if timestamp is not None else int(time.time())

        self.key = key

    def serialize(self):
        return {
            "top": self.top,
            "middle": self.middle,
            "bot": self.bot,
            "last": self.last,
            "signal": self.signal,
            "comment": self.comment,
            "timestamp": self.timestamp,
            "key": self.key,
        }

    @classmethod
    def process(cls, conf: BollingerBandsConf, candles: List[OHLCV]) -> Optional[BaseSignal]:
        key = conf.get_key()

        # top0, middle0, bot0, last0
        res0 = indicators.bollinger_bands(candles, conf.period, conf.multiplier)
        if None in res0:
            return None

        candles1 = candles[:len(candles) - 1]
        res1 = indicators.bollinger_bands(candles1, conf.period, conf.multiplier)
        if None in res1:
            return None

        top0, middle0, bot0, last0 = res0
        top1, middle1, bot1, last1 = res1

        # сигнал на покупку - цена вышла за нижнюю линию, и вернулася назад
        if last1 <= bot1 and last0 > bot0:
            signal = "BUY"
            comment = "Ціна вийшла за нижню лінію і повернулася."
        # на продажу - вышла за верхнюю линию, и вернулася назад
        elif last1 >= top1 and last0 < top0:
            signal = "SELL"
            comment = "Ціна вийшла за верхню лінію і повернулася."
        # пересекла среднюю вверх
        elif last1 <= middle0 < last0:
            signal = "SOFT BUY"
            comment = "Ціна пересікла середню лінію вверх."
        # пересекла среднюю вниз
        elif last1 >= middle0 > last0:
            signal = "SOFT SELL"
            comment = "Ціна пересікла середню лінію вниз."
        else:
            signal = None
            comment = None

        if signal is None:
            signal = BollingerBandsSignal.last_meaningful_signal.get(key, "-")
            comment = BollingerBandsSignal.last_meaningful_comment.get(key, "-")
        else:
            BollingerBandsSignal.last_meaningful_signal[key] = signal
            BollingerBandsSignal.last_meaningful_comment[key] = comment
        return BollingerBandsSignal(top0, middle0, bot0, last0, signal, comment, key)


class StochasticSignal(BaseSignal):
    def __init__(self, k: float, signal: str, comment: str, key: str, timestamp: int = None):
        super().__init__(signal, comment)
        self.k = k
        self.timestamp = timestamp if timestamp is not None else int(time.time())

        self.key = key

    def serialize(self):
        return {
            "k": self.k,
            "signal": self.signal,
            "comment": self.comment,
            "timestamp": self.timestamp,
            "key": self.key
        }

    @classmethod
    def process(cls, conf: StohasticConf, candles: List[OHLCV]) -> Optional["StochasticSignal"]:
        key = conf.get_key()
        k0 = indicators.stochastic_k(candles, conf.period)
        if k0 is None:
            return

        candles1 = candles[:len(candles)-1]
        k1 = indicators.stochastic_k(candles1, conf.period)
        if k1 is None:
            return

        # check sell
        if k1 >= 100 - int(conf.zone_delta) > k0:
            signal = "SELL"
            comment = "Значення повернулася з зони перекупленості."
            # return StochasticSignal(k0, "SELL")
        elif k1 <= int(conf.zone_delta) < k0:
            signal = "BUY"
            comment = "Значення повернулося з зони перепроданості."
            # return StochasticSignal(k0, "BUY")
        else:
            signal = None
            comment = None

        if signal is None:
            signal = StochasticSignal.last_meaningful_signal.get(key, "-")
            comment = StochasticSignal.last_meaningful_comment.get(key, "-")
        else:
            StochasticSignal.last_meaningful_signal[key] = signal
            comment = StochasticSignal.last_meaningful_comment[key] = comment
        return StochasticSignal(k0, signal, comment, key)


class RSISignal(BaseSignal):
    def __init__(self, rsi: float, signal: str, comment: str, key: str, timestamp: int = None):
        super().__init__(signal, comment)
        self.rsi = rsi
        self.timestamp = timestamp if timestamp is not None else int(time.time())

        self.key = key

    def serialize(self):
        return {
            "rsi": self.rsi,
            "signal": self.signal,
            "comment": self.comment,
            "timestamp": self.timestamp,
            "key": self.key
        }

    @classmethod
    def process(cls, conf: RSIConf, candles: List[OHLCV]) -> Optional["RSISignal"]:
        key = conf.get_key()
        rsi0 = indicators.rsi(candles, conf.period)
        if rsi0 is None:
            return

        candles1 = candles[:len(candles)-1]
        rsi1 = indicators.rsi(candles1, conf.period)
        if rsi1 is None:
            return

        # check sell
        if rsi1 >= 100 - int(conf.zone_delta) > rsi0:
            signal = "SELL"
            comment = "Значення повернулася з зони перекупленості."
            # return StochasticSignal(k0, "SELL")
        elif rsi1 <= int(conf.zone_delta) < rsi0:
            signal = "BUY"
            comment = "Значення повернулося з зони перепроданості."
            # return StochasticSignal(k0, "BUY")
        else:
            signal = None
            comment = None

        if signal is None:
            signal = RSISignal.last_meaningful_signal.get(key, "-")
            comment = RSISignal.last_meaningful_comment.get(key, "-")
        else:
            RSISignal.last_meaningful_signal[key] = signal
            comment = RSISignal.last_meaningful_comment[key] = comment
        return RSISignal(rsi0, signal, comment, key)


class SmaCrossSignal(BaseSignal):
    def __init__(self, slow_value: float, fast_value: float, signal: str, comment: str, key: str, timestamp: int = None):
        super().__init__(signal, comment)
        self.fast_value = fast_value
        self.slow_value = slow_value
        self.timestamp = timestamp if timestamp is not None else int(time.time())
        self.key = key

    def serialize(self):
        return {
            "fast_value": self.fast_value,
            "slow_value": self.slow_value,
            "signal": self.signal,
            "comment": self.comment,
            "timestamp": self.timestamp,
            "key": self.key
        }

    @classmethod
    def process(cls, conf: SmaCrossConf, candles: List[OHLCV]) -> Optional["SmaCrossSignal"]:
        key = conf.get_key()
        fast_0 = indicators.sma_point(candles, conf.period_fast)
        slow_0 = indicators.sma_point(candles, conf.period_slow)

        if None in (fast_0, slow_0):
            return None

        candles1 = candles[:len(candles)-1]

        fast_1 = indicators.sma_point(candles1, conf.period_fast)
        slow_1 = indicators.sma_point(candles1, conf.period_slow)

        if None in (fast_1, slow_1):
            return None

        signal = None
        comment = None
        # 1. Швидка пересікла повільну вверх - покупка
        if fast_1 <= slow_1 and fast_0 > slow_0:
            signal = "BUY"
            comment = "Швидка пересікла повільну вверх"

        # 2. Швидка пересікла повільну вниз - продаж
        elif fast_1 >= slow_1 and fast_0 < slow_0:
            signal = "SELL"
            comment = "Швидка пересікла повільну вниз"

        # 3. Обидві направлені вверх - мягка покупка
        elif slow_0 >= slow_1 and fast_0 > fast_1 and fast_0 > slow_0:
            signal = "SOFT BUY"
            comment = "Обидві направлені вверх, швидка вище"

        # 4. Обидві направлені вних - мягка продажа
        elif slow_0 <= slow_0 and fast_0 < fast_1 and fast_0 < slow_0:
            signal = "SOFT SELL"
            comment = "Обидві направлені вниз, швидка нижче"

        if signal is None:
            signal = SmaCrossSignal.last_meaningful_signal.get(key, "-")
            comment = SmaCrossSignal.last_meaningful_comment.get(key, "-")
        else:
            SmaCrossSignal.last_meaningful_signal[key] = signal
            SmaCrossSignal.last_meaningful_comment[key] = comment
        return SmaCrossSignal(slow_0, fast_0, signal, comment, key)

