from typing import List, Optional
import logging
import sys
import math
from data.ohlcv import OHLCV


log = logging.getLogger(__name__)


def rsi(candles: List[OHLCV], period: int, offset: int = 0):
    try:
        candles_count = len(candles)

        if candles_count < offset + period:
            log.error(
                f"{candles[0].pair} {candles[0].market} RSI wrong data: period={period} offset={offset}, {candles_count} candles.")
            return None

        reversed_candles = list(reversed(candles))

        point_candles = reversed_candles[offset:offset + period]

        if len(point_candles) != period:
            log.error(f"SmaPoint: {len(point_candles)} points for period {period}, check calculations!")
            return None

        data = [c.c for c in point_candles]
        up = []
        down = []
        for i in range(len(data) - 1):  # period - 1
            c1 = data[i]
            c2 = data[i + 1]

            if c2 > c1:
                up.append(c2 - c1)
                down.append(0.0)
            elif c2 < c1:
                down.append(c1 - c2)
                up.append(0.0)

        a = period

        up_ema = 0.0
        for i in range(len(up)):
            up_ema = (up[i] + (a - 1) * up_ema) / a

        down_ema = 0.0
        for i in range(1, len(down)):
            down_ema = (down[i] + (a - 1) * down_ema) / a

        if down_ema == 0:
            # log.warning("RSI: down = 0")
            return 100.0

        RS = up_ema / down_ema
        RSI = 100 - (100 / (1 + RS))
        return RSI
        # positive_candles = [c for c in point_candles if float(c.c) > float(c.o)]
        # negative_candles = [c for c in point_candles if float(c.c) < float(c.o)]
        #
        # if len(positive_candles) > 0:
        #     deltas = [float(c.c) - float(c.o) for c in positive_candles]
        #     total_positive = sum(deltas)
        #     total_positive = total_positive / period
        # else:
        #     total_positive = 0
        #
        # if len(negative_candles):
        #     deltas = [float(c.o) - float(c.c) for c in negative_candles]
        #     total_negative = sum(deltas)
        #     total_negative = total_negative / period
        # else:
        #     total_negative = 1
        #
        # rs_value = total_positive / total_negative
        # rsi_value = 100.0 - (100 / (1 + rs_value))
        # return rsi_value
    except Exception:
        log.error(f"{candles[0].pair} {candles[0].market} RSI p={period} o={offset}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")


def sma_point(candles: List[OHLCV], period: int, offset: int = 0):
    try:
        candles_count = len(candles)

        if candles_count < offset + period:
            log.error(f"{candles[0].pair} {candles[0].market} SmaPoint wrong data: period={period} offset={offset}, {candles_count} candles.")
            return None

        reversed_candles = list(reversed(candles))

        point_candles = reversed_candles[offset:offset+period]

        if len(point_candles) != period:
            log.error(f"SmaPoint: {len(point_candles)} points for period {period}, check calculations!")
            return None

        close_values = [c.c for c in point_candles]
        s = sum(close_values)
        return s / period
        # return round(sum(point_candles), 4) / len(point_candles)
    except Exception:
        log.error(f"{candles[0].pair} {candles[0].market} SmaPoint p={period} o={offset}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")


def bollinger_bands(candles: List[OHLCV], period: int, multiplier: int = 2, offset: int = 0) -> \
        (Optional[float], Optional[float], Optional[float], Optional[float]):
    try:
        middle = sma_point(candles, period, offset)
        if middle is None:
            log.error(f"{candles[0].pair} {candles[0].market} BBands middle not counted!")
            return None, None, None, None

        reversed_candles = list(reversed(candles))

        point_candles = reversed_candles[offset:offset+period]

        s = sum([math.pow(c.c - middle, 2) for c in point_candles])

        standard_deviation = math.sqrt(s / (period - 1))
        deviation = multiplier * standard_deviation

        top = middle + deviation
        bot = middle - deviation
        last_price = point_candles[0].c   # reversed candles here

        return top, middle, bot, last_price
    except Exception:
        log.error(f"{candles[0].pair} {candles[0].market} BBands: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
    return None, None, None, None


def stochastic_k(candles: List[OHLCV], period: int, offset: int = 0):
    try:
        candles_count = len(candles)
        reversed_candles = list(reversed(candles))
        point_candles = reversed_candles[offset:offset+period]

        if candles_count < period + offset:
            log.error(f"{candles[0].pair} {candles[0].market} StochK wrong data: period={period} offset={offset}, {candles_count} candles.")
            return None

        high_prices = [c.h for c in point_candles]
        highest_price = max(high_prices)

        l_prices = [c.l for c in point_candles]
        lowest_price = min(l_prices)

        last_price = point_candles[0].c   # reversed here

        k = ((last_price - lowest_price) / (highest_price - lowest_price)) * 100.0
        return k
    except Exception:
        log.error(f"{candles[0].pair} {candles[0].market} StochK: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")
