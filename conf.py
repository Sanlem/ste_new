import os
import pathlib
import yaml

BASE_DIR = pathlib.Path(__file__).parent
config_dir = BASE_DIR / "config"
static_dir = BASE_DIR / "web" / "static"
templates_dir = BASE_DIR / "web" / "templates"


def load_config(file_path):
    with open(file_path) as f:
        config = yaml.safe_load(f)
    return config


logging_config_path = config_dir / "logging.yaml"
logging_config = load_config(logging_config_path)

env = os.environ.get("STE_ENV", "prod")
main_conf_path = config_dir / "{}.yaml".format(env)
config = load_config(main_conf_path)

all_pairs = set()
all_markets = set()
all_timeframes = set()

PAIRS_AVAILABLE = {}
for market, market_conf in config["available"]["ohlcv"].items():
    timeframes = market_conf["timeframes"]
    for pair in market_conf["pairs"]:
        if pair not in PAIRS_AVAILABLE:
            PAIRS_AVAILABLE[pair] = {}

        PAIRS_AVAILABLE[pair][market] = timeframes

        all_pairs.add(pair)
        all_markets.add(market)

        for tf in timeframes:
            all_timeframes.add(tf)

"""
{
    PAIR: {
        MARKET: [TIMEFRAME_LIST]
    }
}

"""