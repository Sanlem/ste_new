from typing import Tuple, Optional
from aiohttp import web
from data.user import User


from aiohttp_session import get_session, Session


async def get_user(request: web.Request) -> Tuple[Optional[User], Session]:
    session = await get_session(request)
    user_id = session.get("user_id", None)

    user = await User.manager.load_one(_id=user_id)
    return user, session


def login_required(f):
    async def wrapper(self):
        session = await get_session(self.request)

        user_id = session.get("user_id", None)

        if user_id is None:
            raise web.HTTPFound(f"/login")

        return await f(self)
    return wrapper
