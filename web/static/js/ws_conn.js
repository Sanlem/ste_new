$(document).ready(function() {
            var host = window.location.host;
            var socket = new WebSocket("ws://" + host + "/ws"),
                $sigCont = $("#signals-container");

            socket.onopen = function() {
                console.log("Соединение установлено.");
//                subscribe();
            };

            socket.onclose = function(event) {
            if (event.wasClean) {
                console.log('Соединение закрыто чисто');
            } else {
                console.log('Обрыв соединения'); // например, "убит" процесс сервера
            }
            console.log('Код: ' + event.code + ' причина: ' + event.reason);
            };

            socket.onmessage = function(event) {
                let msg = JSON.parse(event.data);
                console.log("Получены данные " + event.data);

                processMessage(msg);
            };

            socket.onerror = function(error) {
                console.log("Ошибка " + error.message);
            };

            function processMessage(msg) {
                if (msg.e === "message") {
                    // check if signals; only signals allowed here
                    if (msg.key.includes(pair) && msg.key.includes(market) && !msg.key.includes("strategy")) {
                        processDataMsg(msg);
                    }
                } else if (msg.e === "unsubscribed") {
                    processUnsubscribed(msg);
                }
            };

            function processUnsubscribed(msg) {
                let key = msg.key;
                $("#" + getSigElemId(key)).remove();
            }

            function unsubscribe(key) {
                console.log("Unsubscribe from " + key);
                msg = {
                    e: "unsubscribe",
                    key: key,
                }

                socket.send(JSON.stringify(msg))
            }

            function processDataMsg(msg) {
                let key = msg.key;

                // check if already have such key
                let $entry = $("#" + getSigElemId(key));

                if ($entry.length === 0) {
                    return createEntry(msg);
                } else {
                    return updateEntry(msg);
                }
            }

            function updateEntry(msg) {
                let key = msg.key;
                let sigId = getSigElemId(key);
                let $card = $("#" + sigId);
                let $cardBody = $($card.children(".card-body")[0]);
                let ignoredFields = ["timestamp"];
                let result = msg.result;

                if (typeof result === "object" && result !== null) {
                    for (let resultKey in result) {
                        if (result.hasOwnProperty(resultKey) && !ignoredFields.includes(resultKey)) {
                            let $val = $cardBody.children("." + resultKey);
                            $val.text(resultKey + ": " + result[resultKey]);
                        }

                        if (resultKey == "signal") {
                            if (result[resultKey] == "BUY" || result[resultKey] == "SOFT BUY") {
                                $card.removeClass("sell");
                                $card.addClass("buy");
                            } else if (result[resultKey] == "SELL" || result[resultKey] == "SOFT SELL") {
                                $card.removeClass("buy");
                                $card.addClass("sell");
                            }
                        }
                    }
                }
            }

            function createEntry(msg) {
                let key = msg.key;
                let sigId = getSigElemId(key);
                let ignoredFields = ["timestamp"];

                let $card =  $("<div/>", {
                    id: sigId,
                    class: "col-md-4 signal card"
                });

                let $cardBody = $("<div/>", {
                    class: "card-body",
                });

                $card.append($cardBody);

                let $sigName = $("<h3/>", {
                    class: "text",
                    text: key,
                });

                $cardBody.append($sigName);

                let result = msg.result;

                if (typeof result === "object" && result !== null) {
                    for (let resultKey in result) {
                        if (result.hasOwnProperty(resultKey) && !ignoredFields.includes(resultKey)) {
                            let $val = $("<p/>", {
                                class: "text " + resultKey,
                                text: resultKey + ": " +result[resultKey],
                            });


                            if (resultKey == "signal") {
                                if (result[resultKey] == "BUY" || result[resultKey] == "SOFT BUY") {
                                    $card.removeClass("sell");
                                    $card.addClass("buy");
                                } else if (result[resultKey] == "SELL" || result[resultKey] == "SOFT SELL") {
                                    $card.removeClass("buy");
                                    $card.addClass("sell");
                                }
                            }
                            $cardBody.append($val);
                        }
                    }
                }

                $sigCont.append($card);
                $card.on("dblclick", function(){
                    unsubscribe(key);
                });
            };

            function getSigElemId(key) {
                let sigId = key
                sigId = sigId.replace(/:/g, "-")
                sigId = sigId.replace("/", "")
                return sigId
            }

            var $bb_form = $("#bb_form");
            var $stoch_form = $("#stoch_form");
            var $sma_form = $("#sma_form");
            var $rsi_form = $("#rsi_form");

            $bb_form.on("submit", function(e) {
                e.preventDefault();
                let data = $(this).serializeArray();
                let msg = {};

                data.forEach(function(item, index) {
                    msg[item.name] = item.value;
                });

                msg["e"] = "subscribe";
                msg["channel"] = "BollingerBands";
                msg["market"] = market;
                msg["pair"] = pair;

                console.log("Send " + JSON.stringify(msg))
                socket.send(JSON.stringify(msg));
            });

            $stoch_form.on("submit", function(e) {
                e.preventDefault();
                let data = $(this).serializeArray();
                let msg = {};

                data.forEach(function(item, index) {
                    msg[item.name] = item.value;
                });

                msg["e"] = "subscribe";
                msg["channel"] = "Stochastic";
                msg["market"] = market;
                msg["pair"] = pair;

                console.log("Send " + JSON.stringify(msg))
                socket.send(JSON.stringify(msg));
            });

            $sma_form.on("submit", function(e) {
                e.preventDefault();
                let data = $(this).serializeArray();
                let msg = {};

                data.forEach(function(item, index) {
                    msg[item.name] = item.value;
                });

                msg["e"] = "subscribe";
                msg["channel"] = "sma";
                msg["market"] = market;
                msg["pair"] = pair;

                console.log("Send " + JSON.stringify(msg))
                socket.send(JSON.stringify(msg));
            });

            $rsi_form.on("submit", function(e) {
                e.preventDefault();
                let data = $(this).serializeArray();
                let msg = {};

                data.forEach(function(item, index) {
                    msg[item.name] = item.value;
                });

                msg["e"] = "subscribe";
                msg["channel"] = "rsi";
                msg["market"] = market;
                msg["pair"] = pair;

                console.log("Send " + JSON.stringify(msg))
                socket.send(JSON.stringify(msg));
            });
        });