import logging
import sys
from aiohttp import web
from aiohttp_jinja2 import template
from aiohttp_flashbag import flashbag_get, flashbag_set
from web.utlis import get_user, login_required
from data.strategy import StrategyConf
from data.order import Order
from .ws_handler import CH_CONF
from conf import all_timeframes, all_markets, all_pairs


router = web.RouteTableDef()
log = logging.getLogger(__name__)


@router.view("/strategies/{strategy_id}/delete")
class StrategyDeleteView(web.View):
    @login_required
    async def post(self):
        strategy_id = self.request.match_info["strategy_id"]
        user, session = await get_user(self.request)

        strategy = await StrategyConf.manager.load_one(_id=strategy_id, user_id=user._id)
        if strategy is None:
            raise web.HTTPNotFound()

        await self.request.app["strategies_manager"].remove_strategy(strategy_id)
        await strategy.delete()
        flashbag_set(self.request, "strategies_success", "Відписано.")
        raise web.HTTPFound("/strategies")


@router.view("/strategies/{strategy_id}/")
class StrategyDetailView(web.View):
    @login_required
    @template("strategy_detail.html")
    async def get(self):
        strategy_id = self.request.match_info["strategy_id"]
        user, session = await get_user(self.request)

        strategy = await StrategyConf.manager.load_one(_id=strategy_id, user_id=user._id)
        if strategy is None:
            raise web.HTTPNotFound()

        orders = await Order.manager.load(strategy_id=strategy_id)
        strategy.orders = orders

        s1_change, s2_change = await strategy.calculate_changes()

        # assume only fiat pairs available
        pair = strategy.conf["pair"]
        market = strategy.conf["market"]

        ticker = self.request.app["ticker_provider"].get_last(market, pair)
        if ticker is not None:
            total = float(s1_change) / float(ticker.last) + float(s2_change)
        else:
            total = "-"

        strategy.profit_s2 = total

        return {"user": user, "strategy": strategy}


@router.view("/strategies", name="strategies")
class StrategiesView(web.View):
    @login_required
    @template("strategies.html")
    async def get(self):
        user, session = await get_user(self.request)

        strategies = await StrategyConf.manager.load(user_id=user._id)

        error = flashbag_get(self.request, "strategies_error")
        success = flashbag_get(self.request, "strategies_success")

        sub_dicts = []
        for strategy in strategies:
            sub_type = strategy.sub_type
            if sub_type not in CH_CONF:
                log.warning(f"Unknown sub_type {sub_type}, {strategy}")
                continue

            conf_class = CH_CONF[sub_type]["conf_class"]
            provider_class = CH_CONF[sub_type]["provider_class"]
            needed_fields = CH_CONF[sub_type]["needed_fields"]

            try:
                conf_dict = {f: strategy.conf[f] for f in needed_fields}
            except KeyError:
                log.error(f"Missing field for {conf_class.__name__}: {strategy.serialize()} {str(sys.exc_info([1]))}!")
                continue

            conf = conf_class.deserialize(**conf_dict)
            key = conf.get_key()

            s1_change, s2_change = await strategy.calculate_changes()

            # assume only fiat pairs available
            pair = conf.pair
            market = conf.market

            ticker = self.request.app["ticker_provider"].get_last(market, pair)
            if ticker is not None:
                total = float(s1_change) / float(ticker.last) + float(s2_change)
            else:
                total = "-"

            strategy.profit_s2 = total
            sub_dicts.append(strategy)

        return {"error": error, "success": success, "strategies": sub_dicts,
                "pairs": all_pairs, "markets": all_markets, "timeframes": all_timeframes,
                "user": user}

    @login_required
    async def post(self):
        user, session = await get_user(self.request)
        body = await self.request.post()

        sub_type = body.get("channel")
        if sub_type is None:
            raise web.HTTPFound("/strategies")

        conf_class = CH_CONF[sub_type]["conf_class"]
        provider_class = CH_CONF[sub_type]["provider_class"]
        needed_fields = CH_CONF[sub_type]["needed_fields"]

        try:
            conf_dict = {f: body[f] for f in needed_fields}
        except KeyError:
            log.error(f"Missing field for {conf_class.__name__}: {body}!")
            flashbag_set(self.request, "strategies_error", f"Missing field for {conf_class.__name__}: {body}!")
            raise web.HTTPFound("/strategies")

        conf = conf_class.deserialize(**conf_dict)
        if conf is None:
            flashbag_set(self.request, "strategies_error", f"Failed to init {conf_class.__name__} for {body}!")
            raise web.HTTPFound("/strategies")

        existing_conf = await StrategyConf.manager.load_one(user_id=user._id, key=conf.get_key())
        if existing_conf is not None:
            flashbag_set(self.request, "strategies_error", f"Вже підписані на {conf.get_key()}")
            raise web.HTTPFound("/strategies")

        specific_needed_fields = ["name", "fee"]
        try:
            specific_conf_dict = {f: body[f] for f in specific_needed_fields}
        except KeyError:
            log.error(f"Missing field for {conf_class.__name__}: {body}!")
            flashbag_set(self.request, "strategies_error", f"Missing field for strategy: {body} {str(sys.exc_info()[1])}!")
            raise web.HTTPFound("/strategies")

        name = specific_conf_dict["name"]
        open_on_soft = body.get("open_on_soft")
        close_on_soft = body.get("close_on_soft")

        open_on_soft = open_on_soft if open_on_soft is not None else False
        close_on_soft = close_on_soft if close_on_soft is not None else False

        strategy = StrategyConf(name, user._id, sub_type, conf_dict, f"{sub_type}:{conf.get_key()}",
                                open_on_soft, close_on_soft)

        await strategy.save()

        flashbag_set(self.request, "strategies_success", f"Підписано на {name}.")
        await self.request.app["strategies_manager"].add_strategy(strategy)
        raise web.HTTPFound("/strategies")
