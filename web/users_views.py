from aiohttp import web
from aiohttp_jinja2 import template
from aiohttp_flashbag import flashbag_get, flashbag_set
from data.user import User
from web.utlis import get_user, login_required
from conf import static_dir

router = web.RouteTableDef()
router.static("/static", static_dir)


@router.view("/register", name="register")
class RegisterView(web.View):
    @template("register.html")
    async def get(self):
        user, session = await get_user(self.request)

        if user is not None:
            raise web.HTTPFound("/")

        error = flashbag_get(self.request, "reg_error")
        return {"error": error}

    @template("reg_success.html")
    async def post(self):
        user, session = await get_user(self.request)

        if user is not None:
            flashbag_set(self.request, "reg_error",
                         "Вийдіть із системи перед тим як реєструвати новий аккаунт.")
            raise web.HTTPFound("/")

        body = await self.request.post()

        login = body.get("login")
        password = body.get("password")
        password1 = body.get("password1")

        if None in (login, password, password1):
            flashbag_set(self.request, "reg_error", "Всі поля необхідно заповнити.")
            raise web.HTTPFound("/register")

        user = await User.manager.load_one(login=login)
        if user is not None:
            flashbag_set(self.request, "reg_error", "Такий користувач вже існує.")
            raise web.HTTPFound("register")

        if password and password == password1:
            user = await User.manager.create_user(login, password)
            return {"login": login}
        else:
            flashbag_set(self.request, "reg_error", "Паролі не співпадають.")
            raise web.HTTPFound("/register")


@router.view("/login", name="login")
class LoginView(web.View):
    @template("login.html")
    async def get(self):
        user, session = await get_user(self.request)

        if user is not None:
            raise web.HTTPFound("/")

        error = flashbag_get(self.request, "login_error")
        return {"error": error}

    async def post(self):
        user, session = await get_user(self.request)
        if user is not None:
            raise web.HTTPFound("/")

        body = await self.request.post()

        login = body.get("login")
        password = body.get("password")

        user = await User.manager.load_one(login=login)

        if user is None or user.check_password(password) is False:
            flashbag_set(self.request, "login_error", "Неправильний логін чи пароль.")
            raise web.HTTPFound("/login")

        session["user_id"] = user._id
        raise web.HTTPFound("/")


# @router.view("/", name="index")
# class IndexView(web.View):
#     @login_required
#     @template("index.html")
#     async def get(self):
#         user, session = await get_user(self.request)
#         return {"user": user}


@router.view("/logout", name="logout")
class LogoutView(web.View):
    async def get(self):
        user, session = await get_user(self.request)

        if user is not None:
            del session["user_id"]

        return web.HTTPFound("/login")


@router.view("/me", name="profile")
class ProfileView(web.View):
    @login_required
    @template("me.html")
    async def get(self):
        user, _ = await get_user(self.request)
        success_message = flashbag_get(self.request, "success")
        error_message = flashbag_get(self.request, "error")

        return {"user": user, "success": success_message, "error": error_message}


@router.view("/change_password", name="change_password")
class ChangePasswordView(web.View):
    @login_required
    @template("change_password.html")
    async def get(self):
        user, _ = await get_user(self.request)
        error = flashbag_get(self.request, "ch_error")

        return {"user": user, "error": error}

    @login_required
    @template("change_password.html")
    async def post(self):
        user, session = await get_user(self.request)

        if user is None:
            raise RuntimeError(f"ChangePassword POST user is None!!!")

        body = await self.request.post()

        current_password = body.get("current_password")
        password = body.get("password")
        password1 = body.get("password1")

        if None in (current_password, password, password1):
            flashbag_set(self.request, "ch_error", "Всі поля необхідно заповнити.")
            raise web.HTTPFound("/change_password")

        if user.check_password(current_password) is False:
            flashbag_set(self.request, "ch_error", "Невірний поточний пароль.")
            raise web.HTTPFound("/change_password")

        if password and password == password1:
            user.change_password(password)
            await user.save()
            flashbag_set(self.request, "success", "Пароль успішно змінено.")
            return web.HTTPFound("/me")
        else:
            flashbag_set(self.request, "ch_error", "Паролі не співпадають.")
            raise web.HTTPFound("/change_password")
