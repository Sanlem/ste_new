import logging
from aiohttp import web
from aiohttp_jinja2 import template
from aiohttp_flashbag import flashbag_get, flashbag_set
from data.user import User
from web.utlis import get_user, login_required
from data.notification_subscription import NotificationSubscription
from .ws_handler import CH_CONF
from conf import all_timeframes, all_markets, all_pairs


router = web.RouteTableDef()
log = logging.getLogger(__name__)


@router.view("/notifications/{notification_id}/delete")
class NotificationDeleteView(web.View):
    @login_required
    async def post(self):
        subscr_id = self.request.match_info["notification_id"]
        user, session = await get_user(self.request)

        subscription = await NotificationSubscription.manager.load_one(_id=subscr_id, user_id=user._id)
        if subscription is None:
            raise web.HTTPNotFound()

        await self.request.app["notifications_manager"].remove_notification(subscription)
        flashbag_set(self.request, "notifications_success", "Відписано.")
        raise web.HTTPFound("/notifications")


@router.view("/notifications", name="notifications")
class NotificationsView(web.View):
    @login_required
    @template("notifications.html")
    async def get(self):
        user, session = await get_user(self.request)

        subscriptions = await NotificationSubscription.manager.load(user_id=user._id)
        if user.telegram_id is None:
            return {"error": "Прив'яжіть акаунт в Телеграмі.", "success": None, "notifications": [],
             "pairs": all_pairs, "markets": all_markets, "timeframes": all_timeframes,
             "user": user}

        error = flashbag_get(self.request, "notifications_error")
        success = flashbag_get(self.request, "notifications_success")

        sub_dicts = []
        for subscription in subscriptions:
            sub_type = subscription.sub_type
            if sub_type not in CH_CONF:
                log.warning(f"Unknown sub_type {sub_type}, {subscription}")
                continue

            conf_class = CH_CONF[sub_type]["conf_class"]
            provider_class = CH_CONF[sub_type]["provider_class"]
            needed_fields = CH_CONF[sub_type]["needed_fields"]

            try:
                conf_dict = {f: subscription.conf[f] for f in needed_fields}
            except KeyError:
                log.error(f"Missing field for {conf_class.__name__}: {subscription.serialize()}!")
                continue

            conf = conf_class.deserialize(**conf_dict)
            key = conf.get_key()

            sub_dicts.append({
                "_id": subscription._id,
                "key": key
            })

        return {"error": error, "success": success, "notifications": sub_dicts,
                "pairs": all_pairs, "markets": all_markets, "timeframes": all_timeframes,
                "user": user}

    @login_required
    async def post(self):
        user, session = await get_user(self.request)
        body = await self.request.post()

        if user.telegram_id is None:
            flashbag_set(self.request, "notifications_error", "Прив'яжіть акаунт в Телеграмі.")
            raise web.HTTPFound("/notifications")

        channel = body.get("channel")

        sub_type = body.get("channel")
        if channel is None:
            raise web.HTTPFound("/notifications")

        conf_class = CH_CONF[sub_type]["conf_class"]
        provider_class = CH_CONF[sub_type]["provider_class"]
        needed_fields = CH_CONF[sub_type]["needed_fields"]

        try:
            conf_dict = {f: body[f] for f in needed_fields}
        except KeyError:
            log.error(f"Missing field for {conf_class.__name__}: {body}!")
            flashbag_set(self.request, "notifications_error", f"Missing field for {conf_class.__name__}: {body}!")
            raise web.HTTPFound("/notifications")

        conf = conf_class.deserialize(**conf_dict)
        if conf is None:
            flashbag_set(self.request, "notifications_error", f"Failed to init {conf_class.__name__} for {body}!")
            raise web.HTTPFound("/notifications")

        existing_conf = await NotificationSubscription.manager.load_one(user_id=user._id, key=conf.get_key())
        if existing_conf is not None:
            flashbag_set(self.request, "notifications_error", f"Вже підписані на {conf.get_key()}")
            raise web.HTTPFound("/notifications")
        subscription = NotificationSubscription(user._id, sub_type, conf_dict, "telegram", conf.get_key())
        await subscription.save()

        flashbag_set(self.request, "notifications_success", f"Підписано на {conf.get_key()}.")
        await self.request.app["notifications_manager"].add_notification(subscription)
        raise web.HTTPFound("/notifications")
