import logging
import sys
from asyncio import sleep, get_event_loop
from typing import List
from json import JSONDecodeError
from asyncio import get_event_loop, Queue
from aiohttp import web as aiohttp_web, WSMsgType, WSMessage
from data.subscription import Subscription
from signals.signal_confs import BollingerBandsConf, StohasticConf, SmaCrossConf, RSIConf
from signals.providers import BollingerBandsProvider, StochasticProvider, SmaCrossProvider, RSIProvider
from signals import signals
from web.utlis import get_user


log = logging.getLogger(__name__)
router = aiohttp_web.RouteTableDef()

subscription_type_to_provider = {
    BollingerBandsProvider.subscription_type: {
        "provider_class": BollingerBandsProvider,
        "conf_class": BollingerBandsConf,
    },
    SmaCrossProvider.subscription_type: {
        "provider_class": SmaCrossProvider,
        "conf_class": SmaCrossConf,
    },
    StochasticProvider.subscription_type: {
        "provider_class": StochasticProvider,
        "conf_class": StohasticConf,
    },

    RSIProvider.subscription_type: {
        "provider_class": RSIProvider,
        "conf_class": RSIConf,
    },
}


@router.get("/ws", name="ws")
async def ws_handler(request: aiohttp_web.Request):
    try:
        handler = WsHandler(request)

        await handler.run()

        return handler.ws
    except Exception:
        log.error(f"WsHandlerFunc: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")


CH_CONF = {
    "bollingerbands": {
        "provider_class": BollingerBandsProvider,
        "conf_class": BollingerBandsConf,
        "needed_fields": ["pair", "market", "timeframe", "period", "multiplier"],
        "subscription_type": BollingerBandsProvider.subscription_type,
    },

    "sma": {
        "provider_class": SmaCrossProvider,
        "conf_class": SmaCrossConf,
        "needed_fields": ["pair", "market", "timeframe", "period1", "period2"],
        "subscription_type": SmaCrossProvider.subscription_type,
    },

    "stochastic": {
        "provider_class": StochasticProvider,
        "conf_class": StohasticConf,
        "needed_fields": ["pair", "market", "timeframe", "period", "zone_delta"],
        "subscription_type": StochasticProvider.subscription_type
    },

    "rsi": {
        "provider_class": RSIProvider,
        "conf_class": RSIConf,
        "needed_fields": ["pair", "market", "timeframe", "period", "zone_delta"],
        "subscription_type": RSIProvider.subscription_type
    }
}


class WsHandler(object):
    def __init__(self, request: aiohttp_web.Request):
        self.request = request
        self.app = request.app
        self.ws = None
        self.user = None
        self.loop = get_event_loop()
        self.out_queue = Queue()
        self.in_queue = Queue()

        self.read_messages_task = None
        self.send_messages_task = None
        self.process_in_queue_task = None

        self.should_stop = False

        self.subscriptions = {}
        self.subscriptions_db_objects = {}
        self.app["ws_connections"].add(self)

    async def send(self, message):
        self.loop.create_task(self.out_queue.put(message))

    def get_subscription_channel(self, out_object):
        if isinstance(out_object, signals.BollingerBandsSignal):
            return "BollingerBands"
        elif isinstance(out_object, signals.StochasticSignal):
            return "Stochastic"
        elif isinstance(out_object, signals.SmaCrossSignal):
            return "SmaCross"

    async def process_in_queue(self):
        while self.should_stop is False:
            try:
                in_object = await self.in_queue.get()
                self.in_queue.task_done()

                if not isinstance(in_object, dict):
                    serialized = in_object.serialize()
                    key = serialized.pop("key")
                    message = {
                        "e": "message",
                        "key": key,
                        "result": serialized,
                    }

                else:
                    message = in_object

                await self.send(message)
                if message["e"] != "message":
                    print(f"Sent {message}")
            except Exception:
                log.error(f"ProcessInQueue: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def send_messages(self):
        while self.should_stop is False:
            try:
                message = await self.out_queue.get()
                self.out_queue.task_done()

                if not isinstance(message, dict):
                    message = message.serialize()

                await self.ws.send_json(message)
            except Exception:
                log.error(f"SendMessages: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    @staticmethod
    def check_needed_fields(message_json: dict, needed_fields: List[str]):
        absent_fields = []
        keys = message_json.keys()
        for field in needed_fields:
            if field not in keys:
                absent_fields.append(field)

        return absent_fields

    @staticmethod
    def error_message(e: str, error_string: str, **kwargs) -> dict:
        err_msg = {
            "e": e,
            "error": error_string
        }

        if kwargs:
            err_msg.update(kwargs)
        return err_msg

    async def subscribe(self, message_json: dict):
        try:
            channel = message_json["channel"]
            signal_str = channel.lower()
            channel_to_sig = CH_CONF.get(signal_str, None)
            if channel_to_sig is None:
                log.error(f"Subscribe: wrong channel {signal_str}.")
                return

            provider_class = channel_to_sig["provider_class"]
            conf_class = channel_to_sig["conf_class"]
            needed_fields = channel_to_sig["needed_fields"]

            absent_fields = self.check_needed_fields(message_json, needed_fields)
            if len(absent_fields) > 0:
                return await self.send(self.error_message("subscribe", f"Missing required fields {absent_fields}",
                                                          channel=channel))

            conf_fields = {field: message_json[field] for field in needed_fields}

            conf = conf_class.deserialize(**conf_fields)
            if conf is None:
                return await self.send(self.error_message("subscribe", f"Wrong conf; contact admin.", channel=channel))

            key = conf.get_key()
            if key in self.subscriptions:
                return await self.send(self.error_message("subscribe", f"already subscribed for {key}!"))

            provider = provider_class(self.in_queue, conf, self.app["ohlcv_provider"])
            self.subscriptions[key] = provider
            self.loop.create_task(provider.run())

            response = {
                "e": "subscribed",
                "channel": channel,
                "key": key,
            }
            await self.send(response)

            subscr = Subscription(self.user._id, provider_class.subscription_type, conf_fields)
            await subscr.save()
            self.subscriptions_db_objects[key] = subscr
        except Exception:
            log.error(f"Subscribe {message_json}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_unsubscribe(self, message_json: dict):
        try:
            needed_fields = ["key"]
            absent_fields = self.check_needed_fields(message_json, needed_fields)

            if len(absent_fields) > 0:
                return await self.send(self.error_message(f"Missing required fields: {str(absent_fields)}"))

            key = message_json["key"]

            if key not in self.subscriptions:
                return await self.send(self.error_message("unsubscribe", f"Missing key {key} in subscriptions"))

            provider = self.subscriptions.pop(key)
            provider.stop()

            subscr_obj = self.subscriptions_db_objects.pop(key)
            await subscr_obj.delete()

            response = {
                "e": "unsubscribed",
                "key": key
            }

            return await self.send(response)
        except Exception:
            log.error(f"ProcessUnsubscribe {message_json}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_subscribe(self, message_json: dict):
        try:
            needed_fields = ["channel"]
            absent_fields = self.check_needed_fields(message_json, needed_fields)

            if len(absent_fields) > 0:
                return await self.send(self.error_message(f"Missing required fields: {str(absent_fields)}"))

            return await self.subscribe(message_json)
        except Exception:
            log.error(f"ProcessSubscribe {message_json}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def process_message(self, message: WSMessage):
        try:
            print(message)
            if message.type == WSMsgType.TEXT:
                try:
                    data_json = message.json()
                except JSONDecodeError:
                    return await self.send(self.error_message("error", "Only JSON messages supported."))

                if "e" not in data_json:
                    await self.send(self.error_message("error", "Missing 'e' key."))
                else:
                    event = data_json["e"].lower()
                    if event == "subscribe":
                        return await self.process_subscribe(data_json)
                    elif event == "unsubscribe":
                        return await self.process_unsubscribe(data_json)
                    else:
                        return await self.send(self.error_message("error", f"Unlnown 'e': {event}"))
            elif message.type in [WSMsgType.CLOSED, WSMsgType.CLOSE, WSMsgType.CLOSING]:
                return await self.on_close()
            elif message.type == WSMsgType.ERROR:
                log.error(f"Ws raised error: {self.ws.exception()}.")
                return await self.on_close()

        except Exception:
            log.error(f"ProcessMessage {message}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def read_messages(self):
        async for message in self.ws:
            try:
                if self.ws.closed:
                    log.info(f"CLLLLLLLLLOOOOOOOOSSSSSEEEE")
                    await self.on_close()
                    return
                await self.process_message(message)
            except Exception:
                log.error(f"ReadMessages: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def load_subscriptions(self):
        try:
            if self.user is None:
                log.error(f"Tried to load subscription for None!")
                return

            subscriptions = await Subscription.manager.load(user_id=self.user._id)

            # just for log
            if len(subscriptions) == 0:
                log.info(f"No subscriptions for {self.user.login}")
                return

            for subscription in subscriptions:
                try:
                    subscr_type = subscription.sub_type
                    conf_class = subscription_type_to_provider[subscr_type]["conf_class"]

                    conf = conf_class.deserialize(**subscription.conf)
                    if conf is None:
                        log.error(f"Failed to init conf for {subscription}")
                        continue

                    provider_class = subscription_type_to_provider[subscr_type]["provider_class"]
                    provider = provider_class(self.in_queue, conf, self.app["ohlcv_provider"])

                    key = conf.get_key()
                    if key not in self.subscriptions:
                        self.subscriptions[key] = provider

                    else:
                        log.warning(f"Try to duplicate subscription {subscription}!")
                        continue

                    self.subscriptions_db_objects[key] = subscription
                    self.loop.create_task(provider.run())
                    log.info(f"Loaded {subscription}!")
                except Exception:
                    log.error("while loading subscription {subscription}!")

        except Exception:
            log.error(f"LoadSubscriptions for {self.user}: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

    async def run(self):
        user, session = await get_user(self.request)
        self.user = user

        if self.user is not None:
            # load all its subscriptions
            await self.load_subscriptions()

        self.ws = aiohttp_web.WebSocketResponse()
        await self.ws.prepare(self.request)

        # self.read_messages_task = self.loop.create_task(self.read_messages())
        self.send_messages_task = self.loop.create_task(self.send_messages())
        self.process_in_queue_task = self.loop.create_task(self.process_in_queue())

        async for message in self.ws:
            try:
                if self.ws.closed:
                    log.info(f"CLLLLLLLLLOOOOOOOOSSSSSEEEE")
                    await self.on_close()
                    return
                await self.process_message(message)
            except Exception:
                log.error(f"ReadMessages: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")

        # while self.should_stop is False:
        #     # just not to finish handler func
        #     await sleep(3)
        log.warning(f"Out of looooooooop")
        return await self.on_close()

    async def on_close(self):
        try:
            # Cleanup
            log.info(f"Closed ws connection, cleanup.")
            self.should_stop = True
            # self.read_messages_task.cancel()
            self.send_messages_task.cancel()
            self.process_in_queue_task.cancel()

            for sub_key, provider in self.subscriptions.items():
                provider.stop()

            self.app["ws_connections"].discard(self)
        except Exception:
            log.error(f"OnClose: {str(sys.exc_info()[0])} {str(sys.exc_info()[1])}")