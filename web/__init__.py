from aiohttp.web import RouteTableDef, Application
from conf import static_dir
from .users_views import router as users_router
from .ws_handler import router as ws_router
from .analytics_views import router as analytics_router
from .notifications_views import router as notifications_router
from .strategies_views import router as strategies_router


router = RouteTableDef()
router.static("/static", static_dir)


def init_routes(app: Application):
    app.add_routes(router)
    app.add_routes(users_router)
    app.add_routes(ws_router)
    app.add_routes(analytics_router)
    app.add_routes(notifications_router)
    app.add_routes(strategies_router)
