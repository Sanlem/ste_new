from aiohttp import web
from aiohttp_jinja2 import template
from aiohttp_flashbag import flashbag_get, flashbag_set
from data.user import User
from data.ohlcv import OHLCV
from web.utlis import get_user, login_required
from conf import PAIRS_AVAILABLE

router = web.RouteTableDef()


@router.view("/", name="index")
class AnalyticsIndex(web.View):
    @login_required
    @template("index.html")
    async def get(self):
        user, session = await get_user(self.request)

        error = flashbag_get(self.request, "analytics_error")
        return {"error": error, "conf": PAIRS_AVAILABLE, "user": user}


@router.view("/ohlcv/{pair}/{market}/{timeframe}", name="ohlcv")
class OHLCVView(web.View):
    @template("ohlcv.html")
    async def get(self):
        _pair = self.request.match_info["pair"]
        _pair = _pair.upper()
        pair = _pair.replace("-", "/")

        if pair not in PAIRS_AVAILABLE:
            flashbag_set(self.request, "analytics_error", f"Пара {pair} недоступна.")
            raise web.HTTPFound("/")

        market = self.request.match_info["market"]
        market = market.lower()

        if market not in PAIRS_AVAILABLE[pair]:
            flashbag_set(self.request, "analytics_error", f"Біржа {market} недоступна для пари {pair}.")
            raise web.HTTPFound("/")

        timeframe = self.request.match_info["timeframe"]
        candles = await OHLCV.manager.load(pair=pair, market=market, timeframe=timeframe)

        candles.reverse()
        return {"candles": candles, "pair": pair, "market": market}


@router.view("/{pair}/{market}", name="analytics")
class AnalyticsPairView(web.View):
    @login_required
    @template("pair_analytics.html")
    async def get(self):
        _pair = self.request.match_info["pair"]
        _pair = _pair.upper()
        pair = _pair.replace("-", "/")

        if pair not in PAIRS_AVAILABLE:
            flashbag_set(self.request, "analytics_error", f"Пара {pair} недоступна.")
            raise web.HTTPFound("/")

        market = self.request.match_info["market"]
        market = market.lower()

        if market not in PAIRS_AVAILABLE[pair]:
            flashbag_set(self.request, "analytics_error", f"Біржа {market} недоступна для пари {pair}.")
            raise web.HTTPFound("/")

        timeframes = PAIRS_AVAILABLE[pair][market]
        user, session = await get_user(self.request)
        return {"user": user, "pair": pair, "market": market, "timeframes": timeframes}
